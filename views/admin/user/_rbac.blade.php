<?php
$allowed_roles = config('smorken/rbac::config.allowed_roles', ['admin']);
$params = $filter->toArray();
if (rbac()->isSuperAdmin()) :
    array_unshift($allowed_roles, 'super-admin');
endif;
?>
<form method="post" action="{{ action([$controller, 'doUpdate'], array_replace($params, ['id' => $model->id])) }}">
    @csrf
    <div class="body mt-2 mb-2 pt-2 pb-2 border-top border-bottom">
        <div class="row mb-2">
            <div class="col-2">Roles
                @if ($model->hasRole('super-admin', false))
                    <span class="text-primary">[SA]</span>
                @endif
            </div>
            <div class="col">
                @foreach(rbac()->all() as $role)
                    @if (in_array($role->role_name, $allowed_roles))
                        <div class="form-check">
                            <label for="role-{{ $role->id }}" class="form-check-label">
                                <input type="hidden" name="role[{{ $role->id }}]" value="0">
                                <input type="checkbox" name="role[{{ $role->id }}]" value="1"
                                       class="form-check-input"
                                       id="role-{{ $role->id }}" {{ $model->hasRole($role->id, false) ? 'checked' : null }}>
                                {{ $role->description }}
                            </label>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="{{ action([$controller, 'index'], $filter->except(['id'])) }}" title="Cancel" class="btn btn-outline-warning">Cancel</a>
    </div>
</form>
