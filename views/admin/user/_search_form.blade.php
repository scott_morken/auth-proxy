<?php
$filtered = 'border border-success';
$roles = $roles ?? null;
?>
<form id="filter-form" method="get">
    <div class="card card-body">
        <div class="form-row row g-3 align-items-center">
            <div class="form-group col-md">
                <label for="id" class="sr-only visually-hidden">ID</label>
                <input type="text" name="id" value="{{ $filter->id }}" placeholder="ID (3...)"
                       class="form-control {{ $filter->id ? $filtered : null }}"
                       maxlength="20">
            </div>
            <div class="form-group col-md">
                <label for="username" class="sr-only visually-hidden">Username</label>
                <input type="text" name="username" value="{{ $filter->username }}"
                       placeholder="Username (MEID)"
                       class="form-control {{ $filter->username ? $filtered : null }}"
                       maxlength="20">
            </div>
            <div class="form-group col-md">
                <label for="first_name" class="sr-only visually-hidden">First Name</label>
                <input type="text" name="first_name" value="{{ $filter->first_name }}" placeholder="First"
                       class="form-control  {{ $filter->first_name ? $filtered : null }}"
                       maxlength="64">
            </div>
            <div class="form-group col-md">
                <label for="last_name" class="sr-only visually-hidden">Last Name</label>
                <input type="text" name="last_name" value="{{ $filter->last_name }}" placeholder="Last"
                       class="form-control  {{ $filter->last_name ? $filtered : null }}"
                       maxlength="64">
            </div>
            @if ($roles)
                <div class="form-group col-md">
                    <label for="role" class="sr-only visually-hidden">Role</label>
                    <select name="role"
                            class="form-select {{ !empty($filter->role) ? $filtered : null }}">
                        <option value="">-- Select role --</option>
                        @foreach ($roles as $role)
                            <option value="{{ $role->level }}" {{ $filter->role && strlen($filter->role) && $filter->role == $role->level ? 'selected' : ''}}>
                                {{ $role->descr }}
                            </option>
                        @endforeach
                    </select>
                </div>
            @endif
            <div class="col-md">
                <button type="submit" name="search-button" value="search" class="btn btn-outline-primary">Search</button>
                @if (isset($reset))
                    <a href="{{ $reset }}" title="Reset" class="btn btn-outline-danger">Reset</a>
                @endif
            </div>
        </div>
    </div>
</form>
