@extends('layouts.app')
@section('content')
    <div class="mt-2">
        <a href="{{ action([$controller, 'search']) }}"
           title="Search for new user" class="btn btn-sm btn-outline-success float-right float-end">
            New User
        </a>
    </div>
    <h4>Users</h4>
    @if (method_exists($user, 'getRoleHandler'))
        @php $roles = $user->getRoleHandler() ? $user->getRoleHandler()->all() : null; @endphp
    @endif
    @include('auth-proxy::admin.user._search_form', ['roles' => $roles])
    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_replace($filter->except(['id']), ['id' => $model->id]); ?>
                <tr>
                    <td>
                        <a href="{{ action([$controller, 'view'], $params) }}"
                           title="View User {{ $model->id }}">
                            {{ $model->id }}
                        </a>
                    </td>
                    <td>{{ $model->username }}</td>
                    <td>{{ $model->first_name }}</td>
                    <td>{{ $model->last_name }}</td>
                    <td>{{ $model->email }}</td>
                    <td class="text-md-right text-md-end">
                        <a href="{{ action([$controller, 'impersonate'], $params) }}"
                           title="Impersonate User {{ $model->id }}" class="btn btn-sm btn-outline-primary me-3">
                            Impersonate
                        </a>
                        <a href="{{ action([$controller, 'delete'], $params) }}"
                           title="Delete User {{ $model->id }}" class="btn btn-sm btn-danger">
                            Delete
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $models->appends($filter->except(['page', 'id']))->links() }}
    @else
        <div class="text-muted">No users found.</div>
    @endif
@stop
