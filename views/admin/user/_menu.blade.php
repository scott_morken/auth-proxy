<div class="mt-2 mb-2">
    <a href="{{ action([$controller, 'index'], isset($filter) ? $filter->except(['id']) : []) }}"
       title="Back to list">
        Back to list
    </a>
</div>
