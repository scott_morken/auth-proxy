@extends('layouts.app')
@section('content')
    @include('auth-proxy::admin.user._menu')
    <h3>The following record(s) will be deleted:</h3>
    <div class="card card-body mb-2">
        {{ $model }}
    </div>
    <form method="post" action="{{ action([$controller, 'deleteDelete'], array_replace($filter->except(['id']), ['id' => $model->id])) }}">
        @method('DELETE')
        @csrf
        <div class="button-group">
            <button type="submit" name="Delete" class="btn btn-danger">Delete</button>
            <a href="{{ action([$controller, 'index'], $filter->except(['id'])) }}" title="Cancel delete"
               class="btn btn-outline-warning">Cancel</a>
        </div>
    </form>
@stop
