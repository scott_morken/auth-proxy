@extends('layouts.app')
@section('content')
    <h4>User #{{ $model->id }}</h4>
    @include('auth-proxy::admin.user._menu')
    <div class="row mb-2">
        <div class="col-2">
            ID
        </div>
        <div class="col">{{ $model->id }}</div>
    </div>
    <div class="row mb-2">
        <div class="col-2">
            Username
        </div>
        <div class="col">{{ $model->username }}</div>
    </div>
    <div class="row mb-2">
        <div class="col-2">
            First Name
        </div>
        <div class="col">{{ $model->first_name }}</div>
    </div>
    <div class="row mb-2">
        <div class="col-2">
            Last Name
        </div>
        <div class="col">{{ $model->last_name }}</div>
    </div>
    <div class="row mb-2">
        <div class="col-2">
            Email
        </div>
        <div class="col">{{ $model->email }}</div>
    </div>
    <div class="row mb-2">
        <div class="col-2">
            Extended Data
        </div>
        <div class="col">
            @if ($model->data && is_array($model->data))
                <pre>{{ \Smorken\Support\Arr::stringify($model->data) }}</pre>
            @else
                None
            @endif
        </div>
    </div>
    @if (method_exists($model, 'getRbacProvider'))
        @include('auth-proxy::admin.user._rbac')
    @endif
    @if (method_exists($model, 'getRoleHandler'))
        @include('auth-proxy::admin.user._role')
    @endif
@stop
