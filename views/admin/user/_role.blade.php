<?php
/**
 * @var \Smorken\Roles\Contracts\Role $roleProvider
 */
$roleProvider = $model->getRoleHandler();
if ($roleProvider):
    $model_role = $roleProvider->currentRole($model->id);
endif;
?>
@if ($roleProvider)
    <form method="post"
          action="{{ action([$controller, 'doUpdate'], array_merge(($filter ? $filter->except(['id']) : []), ['id' => $model->id])) }}">
        @csrf
        <div class="body mt-2 mb-2 pt-2 pb-2 border-top border-bottom">
            <div class="row mb-2">
                <div class="col-2">Roles
                    @if ($model->hasRole($roleProvider->getRoleProvider()->getMax()))
                        <span class="text-primary">[SA]</span>
                    @endif
                </div>
                <div class="col">
                    @foreach($roleProvider->all() as $role)
                        <?php $checked = ($model_role && $model_role->id == $role->id); ?>
                        @if ($roleProvider->has($role))
                            <div class="form-check">
                                <label for="role-{{ $role->id }}" class="form-check-label">
                                    <input type="radio" name="role" value="{{ $role->id }}"
                                           class="form-check-input"
                                           id="role-{{ $role->id }}" {{ $checked ? 'checked' : null }}>
                                    {{ $role->descr }}
                                </label>
                            </div>
                        @else
                            <div class="form-check">
                            <span class="">
                                @if ($checked)
                                    <span class="text-success">x</span>
                                @else
                                    <span class="text-muted">-</span>
                                @endif
                                {{ $role->descr }}
                            </span>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div>
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ action([$controller, 'index'], ($filter ? $filter->except(['id']) : [])) }}" title="Cancel"
               class="btn btn-outline-warning">Cancel</a>
        </div>
    </form>
@else
    <div class="text-danger">Role handler not found.</div>
@endif
