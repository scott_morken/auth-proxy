@extends('layouts.app')
@section('content')
    <h4>Find new user</h4>
    @include('auth-proxy::admin.user._menu')
    @include('auth-proxy::admin.user._search_form')
    <h5>Search results</h5>
    @if ($response && $response->isError())
        <div class="alert alert-danger">Error: {{ $response->message }}</div>
    @endif
    @if ($response && $response->hasUsers())
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Username</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($response->users as $model)
            <tr>
                <td>
                    <a href="{{ action([$controller, 'save'], array_replace($filter->except(['id']), ['id' => $model->id])) }}"
                       title="View User {{ $model->id }}">
                        {{ $model->id }}
                    </a>
                </td>
                <td>{{ $model->username }}</td>
                <td>{{ $model->first_name }}</td>
                <td>{{ $model->last_name }}</td>
                <td>{{ $model->email }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @else
        <div class="text-muted">No users found.</div>
    @endif
@stop
