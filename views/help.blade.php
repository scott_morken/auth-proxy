@extends('layouts.app')
@section('content')
    <h2>Login help</h2>
    <p>
        <a href="{{ action([$controller, 'showLoginForm']) }}" title="Back to login">Back to login</a>
    </p>
    <h4>MEID/password help</h4>
    <p>
        Your MEID and password are managed by MCCCD. You can manage your account
        at <a href="https://tools.maricopa.edu" title="Manage your Maricopa account" target="_blank">
            https://tools.maricopa.edu
        </a>
    </p>
    <h4>Help! It's just not working!</h4>
    <p>
        Occasionally, the browser and the server will get upset with each other and refuse
        to play nicely. The usual result is that the login page keeps reappearing when you login without an
        error message to tell you why it's not working. When this happens, the browser's cache needs to be
        completely cleared.
    </p>
    <h5>Google Chrome</h5>
    <ul>
        <li>Click on the 3 stacked dots (or dashes) in the upper right corner.</li>
        <li>Click on the <b>Settings</b> menu item.</li>
        <li>Click on <b>History</b> on the upper left side of the Settings page.</li>
        <li>Click the <b>Clear browsing data..</b> button.</li>
        <li>Make sure that the <b>Obliterate the following items...</b> has <b>the beginning of time</b> selected.</li>
        <li>Check <b>Cookies and other site and plugin data</b> and <b>Cached images and files</b>.</li>
        <li>Click the <b>Clear browsing data</b> button.</li>
    </ul>
    <h5>Mozilla Firefox</h5>
    <ul>
        <li>Click the 3 stacked dashes in the upper right corner.</li>
        <li>Click the <b>History</b> icon.</li>
        <li>Click <b>Clear recent history...</b></li>
        <li>Select <b>Everything</b> from <b>Time range to clear</b>.</li>
        <li>Click the <b>Details</b> button.</li>
        <li>Make sure <b>Cookies, Cache and Active Logins</b> are checked.</li>
        <li>Click the <b>Clear Now</b> button.</li>
    </ul>
    <h5>Microsoft Internet Explorer</h5>
    <ul>
        <li>Click the <b>Tools</b> icon in the upper right (gear icon).</li>
        <li>Select <b>Safety</b> and <b>Delete browsing history...</b> from the menu options.</li>
        <li>Make sure that <b>Cookies and Temporary Internet files</b> are selected.</li>
        <li>Click the <b>Delete</b> button.</li>
    </ul>
@endsection
