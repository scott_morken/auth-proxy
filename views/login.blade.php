@extends('layouts.app')
@section('content')
    <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        @csrf
        <input type="text" name="username" id="username" value="{{ old('username') }}" placeholder="MEID"
               autofocus required maxlength="64" class="form-control"/>
        <input type="password" name="password" id="password" value="" placeholder="password" maxlength="256"
               class="form-control" autocomplete="off" required/>
        <button type="submit" name="login" class="btn btn-primary btn-block btn-lg w-100">Login</button>
        <div style="margin-top: .5em;">
            <a href="{{ action([$controller, 'help']) }}" title="Help logging in">
                Difficulty logging in?
            </a>
        </div>
        <div style="margin: .5em;">
            Only active students, currently employed staff, and designated 3rd parties are authorized
            to access MCCCD and Phoenix College resources.
        </div>
    </form>
@stop
