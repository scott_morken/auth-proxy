<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('users');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->string('id', 72);
                $table->string('username')
                      ->unique();
                $table->string('first_name');
                $table->string('last_name');
                $table->string('email')
                      ->unique();
                $table->timestamp('email_verified_at')
                      ->nullable();
                $table->string('password')->default('*PROXY*');
                $table->rememberToken();
                $table->mediumText('data')
                      ->nullable();
                $table->timestamps();

                $table->primary('id');
                $table->unique('username', 'users_username_ndx');
                $table->index('first_name', 'users_fn_ndx');
                $table->index('last_name', 'users_ln_ndx');
                $table->index('email', 'users_email_ndx');
            });
        } else {
            if (!Schema::hasColumn('users', 'data')) {
                Schema::table('users', function (Blueprint $t) {
                    $t->mediumText('data')
                      ->nullable();
                });
            }
        }
    }
}
