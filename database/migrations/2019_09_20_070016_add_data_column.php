<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDataColumn extends Migration
{

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users')) {
            if (!Schema::hasColumn('users', 'data')) {
                Schema::table('users', function (Blueprint $t) {
                    $t->mediumText('data')
                      ->nullable();
                });
            }
        }
    }
}
