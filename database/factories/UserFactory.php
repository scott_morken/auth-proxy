<?php

namespace Database\Factories\Smorken\Auth\Proxy\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Smorken\Support\Str;

class UserFactory extends Factory
{

    protected $model = User::class;

    public function definition(): array
    {
        return [
            'id' => $this->faker->unique()
                                ->randomNumber(8),
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->safeEmail,
            'username' => Str::random(5).$this->faker->randomNumber(5),
            'password' => 'PROXY',
        ];
    }
}
