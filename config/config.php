<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 1:29 PM
 */
return [
    'debug' => env('APP_DEBUG', false),
    'cache_ttl' => 60 * 60,
    'load_admin_routes' => env('AUTHPROXY_ADMIN_ROUTES', true),
    'load_auth_routes' => env('AUTHPROXY_AUTH_ROUTES', true),
    'admin_route_prefix' => 'admin',
    'admin_route_middleware' => ['web', 'auth', 'can:role-admin'],
    'controllers' => [
        'login' => \Smorken\Auth\Proxy\Http\Controllers\Login\Controller::class,
        'admin' => \Smorken\Auth\Proxy\Http\Controllers\Admin\User\Controller::class,
    ],
    'models' => [
        'response' => \Smorken\Auth\Proxy\Common\Models\Response::class,
        'user' => \Smorken\Auth\Proxy\Common\Models\User::class,
    ],
    'proxies' => [
        \Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectory::class => [
            'group_attribute' => env('AUTHPROXY_GROUP_ATTR', 'memberof'),
            'map' => [
                'id' => env('AUTHPROXY_USER_ID', 'uid'),
                'username' => env('AUTHPROXY_USER_USERNAME', 'samaccountname'),
                'first_name' => env('AUTHPROXY_USER_FIRSTNAME', 'givenname'),
                'last_name' => env('AUTHPROXY_USER_LASTNAME', 'sn'),
                'email' => env('AUTHPROXY_USER_EMAIL', 'mail'),
            ],
            'backend_options' => [
                'bind_filter' => env('LDAP_ACCOUNT_SUFFIX', '%s@domain.edu'),
                'host' => env('LDAP_DOMAIN_CONTROLLER', 'ldap.domain.edu'),
                'base_dn' => env('LDAP_BASE_DN', 'ou=group,dc=domain,dc=org'),
                'bind_user' => env('LDAP_ADMIN_USERNAME', ''),
                'bind_password' => env('LDAP_ADMIN_PASSWORD', ''),
                'client_options' => [
                    'start_tls' => env('LDAP_START_TLS', false),
                    'ssl' => env('LDAP_SSL', true),
                ],
            ],
        ],
    ],
    'active_provider' => \Smorken\Auth\Proxy\Common\Providers\Guzzle::class,
    'provider' => [
        'endpoints' => [
            'authenticate' => env('AUTHPROXY_PROVIDER_AUTH_ENDPOINT', 'http://localhost/authenticate'),
            'search' => env('AUTHPROXY_PROVIDER_SEARCH_ENDPOINT', 'http://localhost/search'),
        ],
        'token' => env('AUTHPROXY_PROVIDER_TOKEN'),
        'host' => env('AUTHPROXY_PROVIDER_HOST'),
        'data' => env('AUTHPROXY_PROVIDER_DATA', 0),
        'backend_options' => [
            'http_errors' => false,
            'connect_timeout' => 5,
            //'verify'        => false,
        ],
    ],
];
