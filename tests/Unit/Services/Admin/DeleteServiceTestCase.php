<?php

namespace Tests\Smorken\Auth\Proxy\Unit\Services\Admin;

use Illuminate\Http\Request;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Smorken\Auth\Proxy\Services\Admin\DeleteService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tests\Smorken\Auth\Proxy\Unit\Services\ServiceBaseTestCase;

class DeleteServiceTestCase extends ServiceBaseTestCase
{

    public function testDeleteFromRequest(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = new DeleteService($this->mockUserProvider());
        $model = new User(['id' => 1, 'foo' => 'bar']);
        $sut->getUserProvider()->shouldReceive('getModel')->andReturn(new User());
        $sut->getUserProvider()->shouldReceive('getById')
            ->once()
            ->with(1)
            ->andReturn($model);
        $sut->getUserProvider()->shouldReceive('delete')
            ->once()
            ->with($model)
            ->andReturn(true);
        $result = $sut->deleteFromRequest($request, 1);
        $this->assertSame($model, $result->model);
        $this->assertTrue($result->result);
        $this->assertEmpty($result->messages);
    }

    public function testDeleteFromRequestModelNotFoundIsException(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = new DeleteService($this->mockUserProvider());
        $model = new User(['id' => 1, 'foo' => 'bar']);
        $sut->getUserProvider()->shouldReceive('getModel')->andReturn(new User());
        $sut->getUserProvider()->shouldReceive('getById')
            ->once()
            ->with(1)
            ->andReturnNull();
        $this->expectException(NotFoundHttpException::class);
        $this->expectExceptionMessage('Resource [1] could not be located');
        $result = $sut->deleteFromRequest($request, 1);
    }
}
