<?php
namespace Smorken\Auth\Proxy\Services\Admin {

    function method_exists($object, $name)
    {
        return true;
    }
}

namespace Tests\Smorken\Auth\Proxy\Unit\Services\Admin {

    use Illuminate\Http\Request;
    use Smorken\Auth\Proxy\Models\Eloquent\User;
    use Smorken\Auth\Proxy\Services\Admin\RoleService;
    use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
    use Tests\Smorken\Auth\Proxy\Unit\Services\ServiceBaseTestCase;

    class RoleServiceTestCase extends ServiceBaseTestCase
    {

        public function testUpdateRoles(): void
        {
            $sut = new RoleService($this->mockUserProvider());
            $user = new User(['id' => 1]);
            $sut->getUserProvider()->shouldReceive('getById')
                ->once()
                ->with(1)
                ->andReturn($user);
            $sut->getUserProvider()->shouldReceive('handleRoleAssignments')
                ->once()
                ->with($user, ['1' => '1'])
                ->andReturn(true);
            $result = $sut->updateRoles(new Request(['role' => ['1' => '1']]), 1);
            $this->assertTrue($result->result);
            $this->assertSame($user, $result->user);
        }

        public function testUpdateRolesMissingModel(): void
        {
            $sut = new RoleService($this->mockUserProvider());
            $sut->getUserProvider()->shouldReceive('getById')
                ->once()
                ->with(1)
                ->andReturn(null);
            $this->expectException(NotFoundHttpException::class);
            $result = $sut->updateRoles(new Request(['role' => ['1' => '1']]), 1);
        }

        public function testUpdateRolesWithFalseResult(): void
        {
            $sut = new RoleService($this->mockUserProvider());
            $user = new User(['id' => 1]);
            $sut->getUserProvider()->shouldReceive('getById')
                ->once()
                ->with(1)
                ->andReturn($user);
            $sut->getUserProvider()->shouldReceive('handleRoleAssignments')
                ->once()
                ->with($user, ['1' => '1'])
                ->andReturn(false);
            $result = $sut->updateRoles(new Request(['role' => ['1' => '1']]), 1);
            $this->assertFalse($result->result);
            $this->assertSame($user, $result->user);
        }
    }
}
