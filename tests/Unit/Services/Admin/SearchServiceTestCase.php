<?php

namespace Tests\Smorken\Auth\Proxy\Unit\Services\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\Auth\Proxy\Common\Models\Response;
use Smorken\Auth\Proxy\Common\Models\User;
use Smorken\Auth\Proxy\Services\Admin\SearchService;
use Smorken\Service\Contracts\Services\FilterService;
use Tests\Smorken\Auth\Proxy\Unit\Services\ServiceBaseTestCase;

class SearchServiceTestCase extends ServiceBaseTestCase
{

    public function testSearchNoSearchButtonDoesNotSearch(): void
    {
        $sut = new SearchService($this->mockProxyProvider(), [
            FilterService::class => new \Smorken\Auth\Proxy\Services\Admin\FilterService(),
        ]);
        $sut->getProxyProvider()->shouldReceive('search')
            ->never();
        $result = $sut->search(new Request(['id' => 1]));
        $expected = [
            'id' => 1,
            'page' => 1,
            'username' => null,
            'last_name' => null,
            'first_name' => null,
            'role' => null,
        ];
        $this->assertEquals($expected, $result->filter->toArray());
        $this->assertNull($result->response);
    }

    public function testSearchNoSearchableParamsDoesNotSearch(): void
    {
        $sut = new SearchService($this->mockProxyProvider(), [
            FilterService::class => new \Smorken\Auth\Proxy\Services\Admin\FilterService(),
        ]);
        $sut->getProxyProvider()->shouldReceive('search')
            ->never();
        $result = $sut->search(new Request(['search-button' => 'search']));
        $expected = [
            'id' => null,
            'page' => 1,
            'username' => null,
            'last_name' => null,
            'first_name' => null,
            'role' => null,
        ];
        $this->assertEquals($expected, $result->filter->toArray());
        $this->assertNull($result->response);
    }

    public function testSearchParamsNotInSearchableParamsDoesNotSearch(): void
    {
        $sut = new SearchService($this->mockProxyProvider(), [
            FilterService::class => new \Smorken\Auth\Proxy\Services\Admin\FilterService(),
        ]);
        $sut->getProxyProvider()->shouldReceive('search')
            ->never();
        $result = $sut->search(new Request(['search-button' => 'search', 'somekey' => 'something']));
        $expected = [
            'id' => null,
            'page' => 1,
            'username' => null,
            'last_name' => null,
            'first_name' => null,
            'role' => null,
        ];
        $this->assertEquals($expected, $result->filter->toArray());
        $this->assertNull($result->response);
    }

    public function testSearchWithSearchableParams(): void
    {
        $sut = new SearchService($this->mockProxyProvider(), [
            FilterService::class => new \Smorken\Auth\Proxy\Services\Admin\FilterService(),
        ]);
        $responseUsers = new Collection([
            new User(['id' => 1, 'first_name' => 'Bar1', 'last_name' => 'Foo1']),
            new User(['id' => 2, 'first_name' => 'Bar2', 'last_name' => 'Foo2']),
        ]);
        $response = (new Response())->fromUsers($responseUsers);
        $sut->getProxyProvider()->shouldReceive('search')
            ->once()
            ->with(['last_name' => 'Foo', 'first_name' => 'Bar'])
            ->andReturn($response);
        $result = $sut->search(new Request(['search-button' => 'search', 'last_name' => 'Foo', 'first_name' => 'Bar']));
        $expected = [
            'id' => null,
            'page' => 1,
            'username' => null,
            'last_name' => 'Foo',
            'first_name' => 'Bar',
            'role' => null,
        ];
        $this->assertEquals($expected, $result->filter->toArray());
        $this->assertSame($response, $result->response);
    }
}
