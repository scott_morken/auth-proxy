<?php

namespace Tests\Smorken\Auth\Proxy\Unit\Services\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\Auth\Proxy\Services\Admin\IndexService;
use Smorken\Service\Contracts\Services\FilterService;
use Tests\Smorken\Auth\Proxy\Unit\Services\ServiceBaseTestCase;

class IndexServiceTestCase extends ServiceBaseTestCase
{

    public function testGetByRequest(): void
    {
        $sut = new IndexService($this->mockUserProvider(), [
            FilterService::class => new \Smorken\Auth\Proxy\Services\Admin\FilterService(),
        ]);
        $filter = [
            'id' => null,
            'username' => null,
            'last_name' => null,
            'first_name' => 'Foo',
            'role' => null,
            'page' => '1',
        ];
        $models = new Collection();
        $sut->getUserProvider()->shouldReceive('getByFilter')
            ->once()
            ->with($filter, 20)
            ->andReturn($models);
        $result = $sut->getByRequest(new Request(['first_name' => 'Foo']));
        $this->assertEquals($filter, $result->filter->toArray());
        $this->assertSame($models, $result->models);
    }
}
