<?php

namespace Tests\Smorken\Auth\Proxy\Unit\Services\Admin;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Mockery as m;
use Smorken\Auth\Proxy\Contracts\Services\Admin\ImpersonateService;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tests\Smorken\Auth\Proxy\Unit\Services\ServiceBaseTestCase;

class ImpersonateServiceTestCase extends ServiceBaseTestCase
{

    public function testImpersonateByGuestUserIsException(): void
    {
        $sut = $this->getSut();
        $sut->getGuard()->shouldReceive('guest')->once()->andReturn(true);
        $this->expectException(AuthorizationException::class);
        $sut->impersonate(new Request(), 1);
    }

    public function testImpersonateFailure(): void
    {
        $sut = $this->getSut();
        $impUser = new User(['id' => 20]);
        $sut->getGuard()->shouldReceive('guest')->once()->andReturn(false);
        $sut->getGuard()->shouldReceive('id')->once()->andReturn(10);
        $sut->getUserProvider()->shouldReceive('getById')
            ->once()
            ->with(20)
            ->andReturn($impUser);
        $sut->getGuard()->shouldReceive('loginUsingId')
            ->once()
            ->with(20);
        $sut->getGuard()->shouldReceive('id')->once()->andReturn(10);
        $result = $sut->impersonate(new Request(), 20);
        $this->assertSame($impUser, $result->impersonatedUser);
        $this->assertEquals(10, $result->originalId);
        $this->assertEquals(10, $result->impersonatedId);
        $this->assertFalse($result->canContinue());
    }

    public function testImpersonateSuccessful(): void
    {
        $sut = $this->getSut();
        $impUser = new User(['id' => 20]);
        $sut->getGuard()->shouldReceive('guest')->once()->andReturn(false);
        $sut->getGuard()->shouldReceive('id')->once()->andReturn(10);
        $sut->getUserProvider()->shouldReceive('getById')
            ->once()
            ->with(20)
            ->andReturn($impUser);
        $sut->getGuard()->shouldReceive('loginUsingId')
            ->once()
            ->with(20);
        $sut->getGuard()->shouldReceive('id')->once()->andReturn(20);
        $result = $sut->impersonate(new Request(), 20);
        $this->assertSame($impUser, $result->impersonatedUser);
        $this->assertEquals(10, $result->originalId);
        $this->assertEquals(20, $result->impersonatedId);
        $this->assertTrue($result->canContinue());
    }

    public function testImpersonateWithMissingUserIsException(): void
    {
        $sut = $this->getSut();
        $sut->getGuard()->shouldReceive('guest')->once()->andReturn(false);
        $sut->getGuard()->shouldReceive('id')->once()->andReturn(10);
        $sut->getUserProvider()->shouldReceive('getById')
            ->once()
            ->with(20)
            ->andReturnNull();
        $this->expectException(NotFoundHttpException::class);
        $result = $sut->impersonate(new Request(), 20);
    }

    protected function getSut(): ImpersonateService
    {
        return new \Smorken\Auth\Proxy\Services\Admin\ImpersonateService($this->mockGuard(), $this->mockUserProvider());
    }

    protected function mockGuard(): Guard
    {
        return m::mock(Guard::class);
    }
}
