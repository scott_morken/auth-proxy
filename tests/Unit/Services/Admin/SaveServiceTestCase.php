<?php

namespace Tests\Smorken\Auth\Proxy\Unit\Services\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\Auth\Proxy\Common\Models\Response;
use Smorken\Auth\Proxy\Common\Models\User;
use Smorken\Auth\Proxy\Services\Admin\SaveService;
use Tests\Smorken\Auth\Proxy\Unit\Services\ServiceBaseTestCase;

class SaveServiceTestCase extends ServiceBaseTestCase
{

    public function testSaveNoResponse(): void
    {
        $sut = new SaveService($this->mockUserProvider(), $this->mockProxyProvider());
        $sut->getProxyProvider()->shouldReceive('search')
            ->once()
            ->with(['id' => 1])
            ->andReturn(new Response());
        $sut->getUserProvider()->shouldReceive('fromResponseUser')
            ->never();
        $result = $sut->save(new Request(), 1);
        $this->assertFalse($result->result);
        $this->assertNull($result->model);
        $this->assertEquals(1, $result->id);
    }

    public function testSaveWithUsers(): void
    {
        $sut = new SaveService($this->mockUserProvider(), $this->mockProxyProvider());
        $responseUsers = new Collection([
            new User([
                'id' => 1, 'username' => 'f1l1', 'first_name' => 'F1', 'last_name' => 'L1',
                'email' => 'f1l1@example.edu',
            ]),
            new User([
                'id' => 2, 'username' => 'f2l2', 'first_name' => 'F2', 'last_name' => 'L2',
                'email' => 'f2l2@example.edu',
            ]),
        ]);
        $response = (new Response())->fromUsers($responseUsers);
        $user = new \Smorken\Auth\Proxy\Models\Eloquent\User($responseUsers[0]->toArray());
        $sut->getProxyProvider()->shouldReceive('search')
            ->once()
            ->with(['id' => 1])
            ->andReturn($response);
        $sut->getUserProvider()->shouldReceive('fromResponseUser')
            ->once()
            ->with($responseUsers[0])
            ->andReturn($user);
        $result = $sut->save(new Request(), 1);
        $this->assertTrue($result->result);
        $this->assertSame($user, $result->model);
        $this->assertEquals(1, $result->id);
    }

    public function testSaveWithUsersEmptyCollection(): void
    {
        $sut = new SaveService($this->mockUserProvider(), $this->mockProxyProvider());
        $responseUsers = new Collection();
        $response = (new Response())->fromUsers($responseUsers);
        $sut->getProxyProvider()->shouldReceive('search')
            ->once()
            ->with(['id' => 1])
            ->andReturn($response);
        $sut->getUserProvider()->shouldReceive('fromResponseUser')
            ->never();
        $result = $sut->save(new Request(), 1);
        $this->assertFalse($result->result);
        $this->assertNull($result->model);
        $this->assertEquals(1, $result->id);
    }
}
