<?php

namespace Tests\Smorken\Auth\Proxy\Unit\Services;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Contracts\Storage\User;

abstract class ServiceBaseTestCase extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    protected function mockProxyProvider(): Provider
    {
        return m::mock(Provider::class);
    }

    protected function mockUserProvider(): User
    {
        $u = m::mock(User::class);
        $u->shouldReceive('getModel')->andReturn(new \Smorken\Auth\Proxy\Models\Eloquent\User());
        return $u;
    }
}
