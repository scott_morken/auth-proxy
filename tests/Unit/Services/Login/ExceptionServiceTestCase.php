<?php

namespace Tests\Smorken\Auth\Proxy\Unit\Services\Login;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\ValidationException;
use Mockery as m;
use Smorken\Auth\Proxy\Common\Exceptions\AuthenticationException;
use Smorken\Auth\Proxy\Common\Exceptions\SystemException;
use Smorken\Auth\Proxy\Services\Login\ExceptionService;
use Tests\Smorken\Auth\Proxy\Unit\Services\ServiceBaseTestCase;

class ExceptionServiceTestCase extends ServiceBaseTestCase
{

    public function testAuthenticationException(): void
    {
        $sut = new ExceptionService($this->mockExceptionHandler());
        $ae = new AuthenticationException('foo bar', 'fiz buz');
        $sut->getExceptionHandler()->shouldReceive('report')
            ->never();
        $result = $sut->handleException($ae);
        $this->assertEquals('fiz buz', $result->message);
    }

    public function testHandleExceptionValidationExceptionCanRethrow(): void
    {
        $sut = new ExceptionService($this->mockExceptionHandler());
        $val = m::mock(Validator::class);
        $val->shouldReceive('errors->all')->andReturn(['foo', 'bar']);
        $val->shouldReceive('getTranslator')
            ->once()
            ->andReturn(new Translator(new ArrayLoader(), 'en'));
        $ve = new ValidationException($val);
        $this->expectExceptionObject($ve);
        $sut->handleException($ve);
    }

    public function testOtherException(): void
    {
        $sut = new ExceptionService($this->mockExceptionHandler());
        $e = new \Exception('foo bar');
        $sut->getExceptionHandler()->shouldReceive('report')
            ->once()
            ->with($e);
        $result = $sut->handleException($e);
        $this->assertEquals('There was an unhandled error during login. Please try again later.', $result->message);
    }

    public function testSystemException(): void
    {
        $sut = new ExceptionService($this->mockExceptionHandler());
        $se = new SystemException('foo bar', 'fiz buz');
        $sut->getExceptionHandler()->shouldReceive('report')
            ->once()
            ->with($se);
        $result = $sut->handleException($se);
        $this->assertEquals('fiz buz', $result->message);
    }

    protected function mockExceptionHandler(): ExceptionHandler
    {
        return m::mock(ExceptionHandler::class);
    }
}
