<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/6/18
 * Time: 9:56 AM
 */

namespace Tests\Smorken\Auth\Proxy\Unit;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Auth\Proxy\AuthProvider;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Common\Exceptions\AuthenticationException;
use Smorken\Auth\Proxy\Common\Exceptions\InvalidException;
use Smorken\Auth\Proxy\Common\Models\Response;
use Smorken\Auth\Proxy\Contracts\Storage\User;

class AuthProviderTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testAuthAttemptCycleNoUserInvalidAuthenticatesOnce(): void
    {
        $creds = ['username' => 'foo', 'password' => 'bar'];
        [$sut, $p, $up] = $this->getSut();
        $response = new Response();
        $response = $response->fromException(new InvalidException('Invalid'));
        $up->shouldReceive('getByUsername')->once()->with('foo')->andReturn(null);
        $p->shouldReceive('authenticate')->once()->with('foo', 'bar')->andReturn($response);
        $up->shouldReceive('fromResponseUser')->never();
        $this->expectException(AuthenticationException::class);
        $this->expectExceptionMessage('Invalid username and/or password.');
        $user = $sut->retrieveByCredentials($creds);
    }

    public function testAuthAttemptCycleNoUserSuccessAuthenticatesOnce(): void
    {
        $creds = ['username' => 'foo', 'password' => 'bar'];
        [$sut, $p, $up] = $this->getSut();
        $response = new Response();
        $response = $response->fromUser(new \Smorken\Auth\Proxy\Common\Models\User(['id' => 1, 'username' => 'foo']),
            true);
        $up->shouldReceive('getByUsername')->once()->with('foo')->andReturn(null);
        $up->shouldReceive('fromResponseUser')->once()->with($response->user)->andReturn($this->getAuthenticatable());
        $p->shouldReceive('authenticate')->once()->with('foo', 'bar')->andReturn($response);
        $user = $sut->retrieveByCredentials($creds);
        $auth = $sut->validateCredentials($user, $creds);
        $this->assertTrue($auth);
    }

    public function testAuthAttemptCycleWithUserAuthenticatesOnce(): void
    {
        $creds = ['username' => 'foo', 'password' => 'bar'];
        [$sut, $p, $up] = $this->getSut();
        $response = new Response();
        $response = $response->fromUser(new \Smorken\Auth\Proxy\Common\Models\User(['id' => 1, 'username' => 'foo']),
            true);
        $up->shouldReceive('getByUsername')->once()->with('foo')->andReturn($this->getAuthenticatable());
        $up->shouldReceive('fromResponseUser')->never();
        $up->shouldReceive('updateFromResponseUser')->once()->with($response->user);
        $p->shouldReceive('authenticate')->once()->with('foo', 'bar')->andReturn($response);
        $user = $sut->retrieveByCredentials($creds);
        $auth = $sut->validateCredentials($user, $creds);
        $this->assertTrue($auth);
    }

    public function testAuthAttemptCycleWithUserInvalidAuthenticatesOnce(): void
    {
        $creds = ['username' => 'foo', 'password' => 'bar'];
        [$sut, $p, $up] = $this->getSut();
        $response = new Response();
        $response = $response->fromException(new InvalidException('Invalid'));
        $up->shouldReceive('getByUsername')->once()->with('foo')->andReturn($this->getAuthenticatable());
        $up->shouldReceive('fromResponseUser')->never();
        $p->shouldReceive('authenticate')->once()->with('foo', 'bar')->andReturn($response);
        $user = $sut->retrieveByCredentials($creds);
        $this->expectException(AuthenticationException::class);
        $this->expectExceptionMessage('Invalid username and/or password.');
        $auth = $sut->validateCredentials($user, $creds);
    }

    protected function getAuthenticatable($attrs = ['id' => 1, 'username' => 'foo']
    ): \Smorken\Auth\Proxy\Contracts\Models\User {
        return (new \Smorken\Auth\Proxy\Models\Eloquent\User())->forceFill($attrs);
    }

    protected function getSut(): array
    {
        $p = m::mock(Provider::class);
        $up = m::mock(User::class);
        $sut = new AuthProvider($p, $up);
        return [$sut, $p, $up];
    }
}
