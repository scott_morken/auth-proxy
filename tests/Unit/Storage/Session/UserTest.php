<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 7:24 AM
 */

namespace Tests\Smorken\Auth\Proxy\Unit\Storage\Session;

use Illuminate\Contracts\Session\Session;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Auth\Proxy\Storage\Session\User;

class UserTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCreate()
    {
        list($sut, $session) = $this->getSut();
        $session->shouldReceive('put')->once()->andReturnUsing(function ($key, $value) {
            $this->assertEquals('auth_user_id_1', $key);
            $this->assertStringContainsString(':38:"Smorken\Auth\Proxy\Models\Session\User":2:{s:2:"id";i:1;s:8:"username";s:3:"foo";}', $value);
        });
        $session->shouldReceive('put')->once()->andReturnUsing(function ($key, $value) {
            $this->assertEquals('auth_user_username_foo', $key);
            $this->assertStringContainsString(':38:"Smorken\Auth\Proxy\Models\Session\User":2:{s:2:"id";i:1;s:8:"username";s:3:"foo";}', $value);
        });
        $user = $sut->create(['id' => 1, 'username' => 'foo']);
        $this->assertEquals(1, $user->id);
        $this->assertEquals('foo', $user->username);
    }

    public function testDelete()
    {
        list($sut, $session) = $this->getSut();
        $session->shouldReceive('remove')->with('auth_user_id_1');
        $session->shouldReceive('remove')->with('auth_user_username_foo');
        $user = $sut->getModel()->newInstance(['id' => 1, 'username' => 'foo']);
        $this->assertTrue($sut->delete($user));
    }

    public function testGetByIdExists()
    {
        list($sut, $session) = $this->getSut();
        $stored = $sut->getModel()->newInstance(['id' => 1, 'username' => 'foo']);
        $serialized = serialize($stored);
        $session->shouldReceive('get')->once()->with('auth_user_id_1')->andReturn($serialized);
        $this->assertEquals($stored->toArray(), $sut->getById(1)->toArray());
    }

    public function testGetByIdNotExists()
    {
        list($sut, $session) = $this->getSut();
        $session->shouldReceive('get')->once()->with('auth_user_id_1')->andReturn(null);
        $this->assertNull($sut->getById(1));
    }

    public function testGetByUsernameExists()
    {
        list($sut, $session) = $this->getSut();
        $stored = $sut->getModel()->newInstance(['id' => 1, 'username' => 'foo']);
        $serialized = serialize($stored);
        $session->shouldReceive('get')->once()->with('auth_user_username_foo')->andReturn($serialized);
        $this->assertEquals($stored->toArray(), $sut->getByUsername('foo')->toArray());
    }

    public function testGetByUsernameNotExists()
    {
        list($sut, $session) = $this->getSut();
        $session->shouldReceive('get')->once()->with('auth_user_username_foo')->andReturn(null);
        $this->assertNull($sut->getByUsername('foo'));
    }

    public function testUpdate()
    {
        list($sut, $session) = $this->getSut();
        $session->shouldReceive('put')->once()->andReturnUsing(function ($key, $value) {
            $this->assertEquals('auth_user_id_1', $key);
            $this->assertStringContainsString(':38:"Smorken\Auth\Proxy\Models\Session\User":2:{s:8:"username";s:3:"foo";s:2:"id";i:1;}', $value);
        });
        $session->shouldReceive('put')->once()->andReturnUsing(function ($key, $value) {
            $this->assertEquals('auth_user_username_foo', $key);
            $this->assertStringContainsString(':38:"Smorken\Auth\Proxy\Models\Session\User":2:{s:8:"username";s:3:"foo";s:2:"id";i:1;}', $value);
        });
        $user = $sut->update(1, ['username' => 'foo']);
        $this->assertEquals(1, $user->id);
        $this->assertEquals('foo', $user->username);
    }

    protected function getSut()
    {
        $session = m::mock(Session::class);
        $sut = new User(new \Smorken\Auth\Proxy\Models\Session\User(), $session);
        return [$sut, $session];
    }
}
