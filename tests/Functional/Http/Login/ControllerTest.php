<?php

namespace Tests\Smorken\Auth\Proxy\Functional\Http\Login;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Exceptions\Handler;
use Mockery as m;
use Psr\Http\Message\ResponseInterface;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Common\Exceptions\AuthenticationException;
use Smorken\Auth\Proxy\Common\Models\User;
use Smorken\Auth\Proxy\Common\Providers\Guzzle;
use Tests\Smorken\Auth\Proxy\TestCase;
use Throwable;

class ControllerTest extends TestCase
{

    protected ?MockHandler $handler = null;

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate');
        $this->mockExceptionHandler();
        $this->mockProvider();
    }

    public function testAttemptLoginNoUser(): void
    {
        $responseModel = $this->newResponse()
                              ->fromException(
                                  new AuthenticationException('Invalid username and/or password.',
                                      'Invalid username and/or password.')
                              );
        $this->addResponse($this->jsonResponse($responseModel));
        $this->visit('/login')
             ->type('foo', 'username')
             ->type('bar', 'password')
             ->press('Login')
             ->seePageIs('/login')
             ->see('Invalid username and/or password.');
    }

    public function testAttemptLoginUserAuthenticated(): void
    {
        $responseModel = $this->newResponse()
                              ->fromUser(new User([
                                  'id' => 1, 'username' => 'foo', 'first_name' => 'Foo', 'last_name' => 'Bar',
                                  'email' => 'foo@example.edu',
                              ]), true);
        $this->addResponse($this->jsonResponse($responseModel));
        $this->visit('/login')
             ->type('foo', 'username')
             ->type('bar', 'password')
             ->press('Login')
             ->seePageIs('/')
             ->see('done!');
    }

    public function testAttemptLoginUserNotAuthenticated(): void
    {
        $responseModel = $this->newResponse()
                              ->fromUser(new User([
                                  'id' => 1, 'username' => 'foo', 'first_name' => 'Foo', 'last_name' => 'Bar',
                                  'email' => 'foo@example.edu',
                              ]), false);
        $this->addResponse($this->jsonResponse($responseModel));
        $this->visit('/login')
             ->type('foo', 'username')
             ->type('bar', 'password')
             ->press('Login')
             ->seePageIs('/login')
             ->see('There was an error authenticating.');
    }

    public function testHelpLink(): void
    {
        $this->visit('/login')
             ->seeLink('Difficulty logging in?')
             ->click('Difficulty logging in?')
             ->seePageIs('/help')
             ->see('Login Help');
    }

    public function testNoPasswordIsValidationError(): void
    {
        $this->visit('/login')
             ->type('foo', 'username')
             ->press('Login')
             ->seePageIs('/login')
             ->see('The password field is required.');
    }

    public function testNoUsernameIsValidationError(): void
    {
        $this->visit('/login')
             ->type('foo', 'password')
             ->press('Login')
             ->seePageIs('/login')
             ->see('The username field is required.');
    }

    protected function addResponse(ResponseInterface $response): void
    {
        $this->handler->append($response);
    }

    protected function jsonResponse(
        \Smorken\Auth\Proxy\Common\Contracts\Models\Response $response
    ): ResponseInterface {
        return new Response($response->getStatus(), [], $response->toJson());
    }

    protected function mockExceptionHandler(bool $show = false): void
    {
        $m = m::mock(Handler::class)->makePartial();
        $m->shouldReceive('report')
          ->andReturnUsing(function (Throwable $e) use ($show) {
              if ($show) {
                  var_dump($e);
              }
          });
        $this->app[ExceptionHandler::class] = $m;
    }

    protected function mockProvider(): void
    {
        $this->handler = new MockHandler();
        $stack = HandlerStack::create($this->handler);
        $client = new Client(['handler' => $stack, 'http_errors' => false]);
        $provider = new Guzzle($client, []);
        $this->app[Provider::class] = $provider;
    }

    protected function newResponse(): \Smorken\Auth\Proxy\Common\Contracts\Models\Response
    {
        return new \Smorken\Auth\Proxy\Common\Models\Response();
    }
}
