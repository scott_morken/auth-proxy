<?php

namespace Tests\Smorken\Auth\Proxy\Functional\Http\Admin\User;

use Database\Seeders\Smorken\Roles\Models\Eloquent\RoleSeeder;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Exceptions\Handler;
use Illuminate\Support\Collection;
use Mockery as m;
use Psr\Http\Message\ResponseInterface;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Common\Providers\Guzzle;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Smorken\Roles\ServiceProvider;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tests\Smorken\Auth\Proxy\TestCase;
use Throwable;

class ControllerTest extends TestCase
{

    protected ?MockHandler $handler = null;

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate');
        $this->seed(RoleSeeder::class);
        ServiceProvider::defineGates($this->app, true);
        $this->mockExceptionHandler();
        $this->mockProvider();
    }

    public function testCreateNewUser(): void
    {
        $responseModel = $this->newResponse()->fromUsers(new Collection([
            new \Smorken\Auth\Proxy\Common\Models\User([
                'id' => '12345', 'first_name' => 'F1', 'last_name' => 'foo1', 'username' => 'f1l1',
                'email' => 'f1l1@example.edu',
            ]),
            new \Smorken\Auth\Proxy\Common\Models\User([
                'id' => '22222', 'first_name' => 'F2', 'last_name' => 'foo2', 'username' => 'f2l2',
                'email' => 'f2l2@example.edu',
            ]),
        ]));
        $this->addResponse($this->jsonResponse($responseModel));
        $responseModelById = $this->newResponse()->fromUsers(new Collection([
            new \Smorken\Auth\Proxy\Common\Models\User([
                'id' => '12345', 'first_name' => 'F1', 'last_name' => 'foo1', 'username' => 'f1l1',
                'email' => 'f1l1@example.edu',
            ]),
        ]));
        $this->addResponse($this->jsonResponse($responseModelById));
        $user = User::factory(['id' => 1])->create();
        (new RoleUser())->create(['role_id' => 1, 'user_id' => $user->id]);
        $this->actingAs($user)
             ->visit('/admin/user')
             ->click('New User')
             ->seePageIs('/admin/user/search')
             ->type('foo', 'last_name')
             ->press('Search')
             ->seePageIs('/admin/user/search?first_name=&id=&last_name=foo&search-button=search&username=')
             ->seeLink('12345')
             ->seeLink('22222')
             ->click('12345')
             ->seePageIs('/admin/user/view/12345');
    }

    public function testCreateNewUserFails(): void
    {
        $responseModel = $this->newResponse()->fromUsers(new Collection([
            new \Smorken\Auth\Proxy\Common\Models\User([
                'id' => '12345', 'first_name' => 'F1', 'last_name' => 'foo1', 'username' => 'f1l1',
                'email' => 'f1l1@example.edu',
            ]),
            new \Smorken\Auth\Proxy\Common\Models\User([
                'id' => '22222', 'first_name' => 'F2', 'last_name' => 'foo2', 'username' => 'f2l2',
                'email' => 'f2l2@example.edu',
            ]),
        ]));
        $this->addResponse($this->jsonResponse($responseModel));
        $responseModelById = $this->newResponse()->fromException(new NotFoundHttpException('nope'));
        $this->addResponse($this->jsonResponse($responseModelById));
        $user = User::factory(['id' => 1])->create();
        (new RoleUser())->create(['role_id' => 1, 'user_id' => $user->id]);
        $this->actingAs($user)
             ->visit('/admin/user')
             ->click('New User')
             ->seePageIs('/admin/user/search')
             ->type('foo', 'last_name')
             ->press('Search')
             ->seePageIs('/admin/user/search?first_name=&id=&last_name=foo&search-button=search&username=')
             ->seeLink('12345')
             ->seeLink('22222')
             ->click('12345')
             ->seePageIs('/admin/user/search?last_name=foo&page=1');
    }

    public function testDeleteUser(): void
    {
        $user = User::factory(['id' => 1])->create();
        (new RoleUser())->create(['role_id' => 1, 'user_id' => $user->id]);
        $user2 = User::factory(['id' => 1234])->create();
        $this->actingAs($user)
             ->visit('/admin/user/delete/'.$user2->id)
             ->see($user2->first_name.' '.$user2->last_name)
             ->press('Delete')
             ->seePageIs('/admin/user?page=1')
             ->dontSee($user2->id);
    }

    public function testImpersonate(): void
    {
        $user = User::factory(['id' => 1])->create();
        (new RoleUser())->create(['role_id' => 1, 'user_id' => $user->id]);
        $user2 = User::factory(['id' => 1234])->create();
        $this->actingAs($user)
             ->visit('/admin/user/impersonate/'.$user2->id)
             ->seePageIs('/')
             ->see('done!');
    }

    public function testIndex(): void
    {
        $user = User::factory(['id' => 1])->create();
        (new RoleUser())->create(['role_id' => 1, 'user_id' => $user->id]);
        $this->actingAs($user)
             ->visit('/admin/user')
             ->see($user->username);
    }

    public function testNoAdminUserIs403(): void
    {
        $user = new User(['id' => 9999]);
        $response = $this->actingAs($user)
                         ->call('GET', '/admin/user');
        $this->assertEquals(403, $response->getStatusCode());
    }

    public function testNotAuthenticatedRequiresLogin(): void
    {
        $this->visit('/admin/user')
             ->seePageIs('/login');
    }

    public function testView(): void
    {
        $user = User::factory(['id' => 1])->create();
        (new RoleUser())->create(['role_id' => 1, 'user_id' => $user->id]);
        $this->actingAs($user)
             ->visit('/admin/user')
             ->click($user->id)
             ->seePageIs('/admin/user/view/'.$user->id.'?page=1')
             ->see('http://localhost/admin/user?page=1')
             ->see($user->username)
             ->seeIsSelected('role', '1');
    }

    public function testViewCanUpdateRole(): void
    {
        $user = User::factory(['id' => 1])->create();
        (new RoleUser())->create(['role_id' => 1, 'user_id' => $user->id]);
        $user2 = User::factory(['id' => 1234])->create();
        $this->actingAs($user)
             ->visit('/admin/user/view/'.$user2->id)
             ->see($user2->username)
             ->dontSee('checked>')
             ->select('3', 'role')
             ->press('Save')
             ->seePageIs('/admin/user?page=1')
             ->visit('/admin/user/view/'.$user2->id)
             ->seeIsSelected('role', '3');
    }

    protected function addResponse(ResponseInterface $response): void
    {
        $this->handler->append($response);
    }

    protected function jsonResponse(
        \Smorken\Auth\Proxy\Common\Contracts\Models\Response $response
    ): ResponseInterface {
        return new Response($response->getStatus(), [], $response->toJson());
    }

    protected function mockExceptionHandler(bool $show = false): void
    {
        $m = m::mock(Handler::class)->makePartial();
        $m->shouldReceive('report')
          ->andReturnUsing(function (Throwable $e) use ($show) {
              if ($show) {
                  var_dump($e);
              }
          });
        $this->app[ExceptionHandler::class] = $m;
    }

    protected function mockProvider(): void
    {
        $this->handler = new MockHandler();
        $stack = HandlerStack::create($this->handler);
        $client = new Client(['handler' => $stack, 'http_errors' => false]);
        $provider = new Guzzle($client, []);
        $this->app[Provider::class] = $provider;
    }

    protected function newResponse(): \Smorken\Auth\Proxy\Common\Contracts\Models\Response
    {
        return new \Smorken\Auth\Proxy\Common\Models\Response();
    }
}
