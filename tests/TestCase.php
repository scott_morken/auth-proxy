<?php

namespace Tests\Smorken\Auth\Proxy;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\BrowserKitTesting\Concerns\ImpersonatesUsers;
use Laravel\BrowserKitTesting\Concerns\InteractsWithAuthentication;
use Laravel\BrowserKitTesting\Concerns\InteractsWithConsole;
use Laravel\BrowserKitTesting\Concerns\InteractsWithContainer;
use Laravel\BrowserKitTesting\Concerns\InteractsWithDatabase;
use Laravel\BrowserKitTesting\Concerns\InteractsWithExceptionHandling;
use Laravel\BrowserKitTesting\Concerns\InteractsWithSession;
use Laravel\BrowserKitTesting\Concerns\MakesHttpRequests;
use Orchestra\Testbench\Concerns\Testing;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Smorken\Auth\Proxy\ServiceProvider;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{

    use ImpersonatesUsers,
        InteractsWithAuthentication,
        InteractsWithConsole,
        InteractsWithContainer,
        InteractsWithDatabase,
        InteractsWithExceptionHandling,
        InteractsWithSession,
        MakesHttpRequests,
        Testing;

    public $baseUrl = 'http://localhost';

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('app.env', 'production');
        $app['config']->set('app.debug', false);
        $app['config']->set('mail.driver', 'log');
        $app['config']->set('database.default', 'testbench');
        $app['config']->set(
            'database.connections.testbench',
            [
                'driver' => 'sqlite',
                'database' => ':memory:',
                'prefix' => '',
            ]
        );
        $app['config']->set('view.paths', [__DIR__.'/../views']);
        $app['config']->set('auth.providers.users.driver', 'proxy');
        $app['config']->set('auth.providers.users.model', User::class);
        $app['config']->set('auth-proxy.load_retired_routes', true);
        Route::get('/', function (Request $request) {
            return 'done!';
        });
    }

    protected function getPackageProviders($app)
    {
        return [
            ServiceProvider::class,
            \Smorken\CacheAssist\ServiceProvider::class,
            \Smorken\Sanitizer\ServiceProvider::class,
            \Smorken\Roles\ServiceProvider::class,
        ];
    }

    /**
     * Refresh the application instance.
     *
     * @return void
     */
    protected function refreshApplication()
    {
        $_ENV['APP_ENV'] = 'testing';

        $this->app = $this->createApplication();
    }

    protected function setUp(): void
    {
        $this->setUpTheTestEnvironment();
    }

    /**
     * Boot the testing helper traits.
     *
     * @return array<string, string>
     */
    protected function setUpTraits()
    {
        $uses = array_flip(class_uses_recursive(static::class));

        return $this->setUpTheTestEnvironmentTraits($uses);
    }

    protected function tearDown(): void
    {
        $this->tearDownTheTestEnvironment();
    }
}
