<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Login;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property string|null $message
 */
interface ExceptionResult extends VOResult
{

}
