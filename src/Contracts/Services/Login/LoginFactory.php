<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Login;

use Smorken\Service\Contracts\Services\Factory;

interface LoginFactory extends Factory
{

}
