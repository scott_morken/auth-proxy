<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Login;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Smorken\Service\Contracts\Services\BaseService;

interface ExceptionService extends BaseService
{

    public function getExceptionHandler(): ExceptionHandler;

    public function handleException(\Throwable $exception): ExceptionResult;
}
