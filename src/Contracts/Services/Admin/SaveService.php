<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\VO\ModelResult;

interface SaveService extends BaseService, HasProxyProvider, HasUserProvider
{

    public function save(Request $request, string|int $id): ModelResult;
}
