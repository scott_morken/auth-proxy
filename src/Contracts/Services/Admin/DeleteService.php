<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

interface DeleteService extends \Smorken\Service\Contracts\Services\DeleteService, HasUserProvider
{

}
