<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;

interface ImpersonateService extends BaseService, HasUserProvider
{

    public function getGuard(): Guard;

    public function impersonate(Request $request, string|int $id): ImpersonateResult;
}
