<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

use Illuminate\Http\Request;

interface RoleService extends HasUserProvider
{

    public function updateRoles(Request $request, string|int $id): RoleResult;
}
