<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property \Smorken\Support\Contracts\Filter $filter
 * @property \Smorken\Auth\Proxy\Common\Contracts\Models\Response|null $response
 */
interface SearchResult extends VOResult
{

}
