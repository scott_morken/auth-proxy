<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

use Smorken\Auth\Proxy\Contracts\Storage\User;

interface HasUserProvider
{

    public function getUserProvider(): User;
}
