<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

interface RetrieveService extends HasUserProvider, \Smorken\Service\Contracts\Services\RetrieveService
{

}
