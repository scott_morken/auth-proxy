<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

use Smorken\Service\Contracts\Services\Factory;

interface AdminFactory extends Factory
{

}
