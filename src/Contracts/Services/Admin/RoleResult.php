<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property \Smorken\Auth\Proxy\Contracts\Models\User $user
 * @property bool $result
 */
interface RoleResult extends VOResult
{

}
