<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

use Smorken\Auth\Proxy\Common\Contracts\Provider;

interface HasProxyProvider
{

    public function getProxyProvider(): Provider;
}
