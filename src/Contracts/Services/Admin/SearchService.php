<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\HasFilterService;

interface SearchService extends BaseService, HasFilterService, HasProxyProvider
{

    public function search(Request $request): SearchResult;
}
