<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

use Smorken\Service\Contracts\Services\HasFilterService;
use Smorken\Service\Contracts\Services\VO\IndexResult;
use Smorken\Support\Contracts\Filter;

interface IndexService extends HasUserProvider, HasFilterService, \Smorken\Service\Contracts\Services\IndexService
{

    public function getByFilter(Filter $filter, int $perPage = 20): IndexResult;
}
