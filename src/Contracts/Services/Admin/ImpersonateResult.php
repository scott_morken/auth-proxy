<?php

namespace Smorken\Auth\Proxy\Contracts\Services\Admin;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property \Smorken\Auth\Proxy\Contracts\Models\User $impersonatedUser
 * @property string|null $impersonatedId
 * @property string $originalId
 * @property string $redirectPath
 */
interface ImpersonateResult extends VOResult
{

    public function canContinue(): bool;
}
