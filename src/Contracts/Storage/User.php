<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 7:01 AM
 */

namespace Smorken\Auth\Proxy\Contracts\Storage;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\CacheAssist\Contracts\CacheAssist;
use Smorken\Support\Contracts\Filter;

interface User
{

    /**
     * @param  array  $data
     * @return \Smorken\Auth\Proxy\Contracts\Models\User
     */
    public function create(array $data): \Smorken\Auth\Proxy\Contracts\Models\User;

    /**
     * @param  \Smorken\Auth\Proxy\Contracts\Models\User  $user
     * @return bool
     */
    public function delete(\Smorken\Auth\Proxy\Contracts\Models\User $user): bool;

    /**
     * @param  \Smorken\Auth\Proxy\Common\Contracts\Models\User  $user
     * @return \Smorken\Auth\Proxy\Contracts\Models\User
     */
    public function fromResponseUser(\Smorken\Auth\Proxy\Common\Contracts\Models\User $user
    ): \Smorken\Auth\Proxy\Contracts\Models\User;

    /**
     * @param  \Smorken\Support\Contracts\Filter|array  $filter
     * @param  int  $per_page
     * @return \Illuminate\Support\Collection|\Illuminate\Contracts\Pagination\Paginator
     */
    public function getByFilter(Filter|array $filter, int $per_page = 20): Collection|Paginator;

    /**
     * @param  string|int  $id
     * @return \Smorken\Auth\Proxy\Contracts\Models\User|null
     */
    public function getById(string|int $id): ?\Smorken\Auth\Proxy\Contracts\Models\User;

    /**
     * @param  string  $username
     * @return \Smorken\Auth\Proxy\Contracts\Models\User|null
     */
    public function getByUsername(string $username): ?\Smorken\Auth\Proxy\Contracts\Models\User;

    public function getCacheAssist(): ?CacheAssist;

    /**
     * @return \Smorken\Auth\Proxy\Contracts\Models\User
     */
    public function getModel(): \Smorken\Auth\Proxy\Contracts\Models\User;

    public function setCacheAssist(CacheAssist $cacheAssist): void;

    /**
     * @param  string|int  $id
     * @param  array  $data
     * @return \Smorken\Auth\Proxy\Contracts\Models\User
     */
    public function update(string|int $id, array $data): \Smorken\Auth\Proxy\Contracts\Models\User;

    /**
     * @param  \Smorken\Auth\Proxy\Common\Contracts\Models\User  $user
     * @return \Smorken\Auth\Proxy\Contracts\Models\User
     */
    public function updateFromResponseUser(\Smorken\Auth\Proxy\Common\Contracts\Models\User $user
    ): \Smorken\Auth\Proxy\Contracts\Models\User;
}
