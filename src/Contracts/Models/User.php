<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 7:00 AM
 */

namespace Smorken\Auth\Proxy\Contracts\Models;

use Illuminate\Contracts\Auth\Authenticatable;

/**
 * Interface User
 *
 * @package Smorken\Auth\Proxy\Contracts\Models
 *
 * @property string $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property array $data
 * @property \Carbon\Carbon|null created_at
 * @property \Carbon\Carbon|null updated_at
 *
 * @property string $shortName
 */
interface User extends Authenticatable
{

    /**
     * @return string
     */
    public function getLoginField(): string;

    /**
     * @return string
     */
    public function shortName(): string;

    /**
     * @return bool
     */
    public function validate(): bool;
}
