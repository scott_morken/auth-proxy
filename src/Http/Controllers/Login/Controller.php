<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 9:48 AM
 */

namespace Smorken\Auth\Proxy\Http\Controllers\Login;

use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Smorken\Auth\Proxy\Contracts\Services\Login\ExceptionService;
use Smorken\Auth\Proxy\Contracts\Services\Login\LoginFactory;

class Controller extends \Smorken\Controller\View\Controller
{

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected string $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @param  \Smorken\Auth\Proxy\Contracts\Services\Login\LoginFactory  $factory
     * @param  \Smorken\Auth\Proxy\Contracts\Storage\User  $user
     */
    public function __construct(
        protected LoginFactory $factory,
        protected \Smorken\Auth\Proxy\Contracts\Storage\User $user
    ) {
        $this->middleware('guest')
             ->except('logout');
        parent::__construct();
    }

    public function help(): \Illuminate\Contracts\View\View
    {
        return $this->makeView('auth-proxy::help');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request): RedirectResponse|Response|JsonResponse
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') && $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        try {
            if ($this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            }
            $err = trans('auth.failed');
        } catch (\Exception $e) {
            $result = $this->getExceptionService()->handleException($e);
            $err = $result->message;
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponseWithMessage($request, $err);
    }

    public function showLoginForm(): View
    {
        return $this->makeView('auth-proxy::login');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username(): string
    {
        return $this->user->getModel()
                          ->getLoginField();
    }

    protected function getExceptionService(): ExceptionService
    {
        return $this->factory->getService(ExceptionService::class);
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $message
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponseWithMessage(Request $request, string $message): void
    {
        throw ValidationException::withMessages([
            $this->username() => [$message],
        ]);
    }
}
