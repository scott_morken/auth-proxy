<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 11:33 AM
 */

namespace Smorken\Auth\Proxy\Http\Controllers\Admin\User;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Smorken\Auth\Proxy\Contracts\Services\Admin\AdminFactory;
use Smorken\Auth\Proxy\Contracts\Services\Admin\DeleteService;
use Smorken\Auth\Proxy\Contracts\Services\Admin\ImpersonateService;
use Smorken\Auth\Proxy\Contracts\Services\Admin\IndexService;
use Smorken\Auth\Proxy\Contracts\Services\Admin\RetrieveService;
use Smorken\Auth\Proxy\Contracts\Services\Admin\RoleService;
use Smorken\Auth\Proxy\Contracts\Services\Admin\SaveService;
use Smorken\Auth\Proxy\Contracts\Services\Admin\SearchService;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Services\VO\RedirectActionResult;

class Controller extends \Smorken\Controller\View\Controller
{

    protected string $baseView = 'auth-proxy::admin.user';

    public function __construct(
        protected AdminFactory $factory
    ) {
        parent::__construct();
    }

    public function delete(Request $request, string|int $id): \Illuminate\Contracts\View\View
    {
        $filter = $this->getFilter($request);
        $result = $this->getRetrieveService()->findById($id);
        return $this->makeView($this->getViewName('delete'))
                    ->with('model', $result->model)
                    ->with('filter', $filter);
    }

    public function deleteDelete(Request $request, string|int $id): RedirectResponse
    {
        $filter = $this->getFilter($request);
        $result = $this->getDeleteService()->deleteFromRequest($request, $id);
        if ($result->result) {
            $request->session()
                    ->flash('flash:success', "Resource [$id] deleted.");
        } else {
            $request->session()
                    ->flash('flash:danger', "Unable to delete resource [$id].");
        }
        return (new RedirectActionResult($this->actionArray('index'), $filter->except(['id'])))->redirect();
    }

    public function doUpdate(Request $request, string|int $id): RedirectResponse
    {
        $result = $this->getRoleService()->updateRoles($request, $id);
        if ($result->result) {
            $request->session()->flash('flash:success', 'Updated user ['.$result->user->shortName().'].');
        }
        return (new RedirectActionResult($this->actionArray('index'),
            $this->getFilter($request)->except(['id', 'role'])))->redirect();
    }

    public function impersonate(Request $request, string|int $id): RedirectResponse
    {
        $result = $this->getImpersonateService()->impersonate($request, $id);
        if ($result->canContinue()) {
            $request->session()
                    ->put('impersonating', $result->impersonatedId);
            return Redirect::to($result->redirectPath);
        }
        $request->session()
                ->flash('flash:warning', "Unable to impersonate [$id].");
        return (new RedirectActionResult($this->actionArray('index')))->redirect();
    }

    public function index(Request $request): \Illuminate\Contracts\View\View
    {
        $result = $this->getIndexService()->getByRequest($request);
        return $this->makeView($this->getViewName('index'))
                    ->with('models', $result->models)
                    ->with('user', $this->getIndexService()->getUserProvider()->getModel())
                    ->with('reset', action($this->actionArray('index')))
                    ->with('filter', $result->filter);
    }

    public function save(Request $request, string|int $id): RedirectResponse
    {
        $result = $this->getSaveService()->save($request, $id);
        if ($result->result) {
            $request->session()
                    ->flash('flash:success', "Saved user [$id].");
            return (new RedirectActionResult($this->actionArray('view'), ['id' => $id]))->redirect();
        }
        $request->session()
                ->flash('flash:danger', 'Unable to save user.');
        return (new RedirectActionResult($this->actionArray('search'),
            $this->getFilter($request)->toArray()))->redirect();
    }

    public function search(Request $request): \Illuminate\Contracts\View\View
    {
        $result = $this->getSearchService()->search($request);
        return $this->makeView($this->getViewName('search'))
                    ->with('response', $result->response)
                    ->with('reset', action($this->actionArray('search')))
                    ->with('filter', $result->filter);
    }

    public function view(Request $request, string|int $id): \Illuminate\Contracts\View\View
    {
        $result = $this->getRetrieveService()->findById($id);
        $filter = $this->getFilter($request);
        return $this->makeView($this->getViewName('view'))
                    ->with('model', $result->model)
                    ->with('filter', $filter);
    }

    protected function getDeleteService(): DeleteService
    {
        return $this->factory->getService(DeleteService::class);
    }

    protected function getFilter(Request $request)
    {
        return $this->getFilterService()->getFilterFromRequest($request);
    }

    protected function getFilterService(): FilterService
    {
        return $this->factory->getService(FilterService::class);
    }

    protected function getImpersonateService(): ImpersonateService
    {
        return $this->factory->getService(ImpersonateService::class);
    }

    protected function getIndexService(): IndexService
    {
        return $this->factory->getService(IndexService::class);
    }

    protected function getRetrieveService(): RetrieveService
    {
        return $this->factory->getService(RetrieveService::class);
    }

    protected function getRoleService(): RoleService
    {
        return $this->factory->getService(RoleService::class);
    }

    protected function getSaveService(): SaveService
    {
        return $this->factory->getService(SaveService::class);
    }

    protected function getSearchService(): SearchService
    {
        return $this->factory->getService(SearchService::class);
    }
}
