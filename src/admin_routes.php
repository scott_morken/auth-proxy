<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

Route::middleware(Config::get('auth-proxy.admin_route_middleware', ['web', 'auth', 'can:role-admin']))
     ->prefix(Config::get('auth-proxy.admin_route_prefix', 'admin'))
     ->group(function () {
         Route::prefix('user')
              ->group(function () {
                  $controller = Config::get('auth-proxy.controllers.admin',
                      \Smorken\Auth\Proxy\Http\Controllers\Admin\User\Controller::class);
                  Route::get('/', [$controller, 'index']);
                  Route::get('view/{id}', [$controller, 'view']);
                  Route::get('impersonate/{id}', [$controller, 'impersonate']);
                  Route::get('search', [$controller, 'search']);
                  Route::get('save/{id}', [$controller, 'save']);
                  Route::post('update/{id}', [$controller, 'doUpdate']);
                  Route::get('delete/{id}', [$controller, 'delete']);
                  Route::delete('delete/{id}', [$controller, 'deleteDelete']);
              });
     });
