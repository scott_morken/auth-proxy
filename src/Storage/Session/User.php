<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 7:17 AM
 */

namespace Smorken\Auth\Proxy\Storage\Session;

use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Collection;
use Smorken\CacheAssist\Contracts\CacheAssist;
use Smorken\Support\Contracts\Filter;

class User implements \Smorken\Auth\Proxy\Contracts\Storage\User
{

    /**
     * @var \Smorken\Auth\Proxy\Models\Session\User
     */
    protected \Smorken\Auth\Proxy\Models\Session\User $model;

    /**
     * @var \Illuminate\Contracts\Session\Session
     */
    protected Session $session;

    public function __construct(\Smorken\Auth\Proxy\Models\Session\User $user, Session $session)
    {
        $this->model = $user;
        $this->session = $session;
    }

    public function create(array $data): \Smorken\Auth\Proxy\Contracts\Models\User
    {
        $user = $this->getModel()
                     ->newInstance($data);
        $this->store($user);
        return $user;
    }

    /**
     * @param  \Smorken\Auth\Proxy\Contracts\Models\User  $user
     * @return bool
     */
    public function delete(\Smorken\Auth\Proxy\Contracts\Models\User $user): bool
    {
        $this->remove($user->id);
        $this->remove($user->username, 'username');
        return true;
    }

    /**
     * @param  \Smorken\Auth\Proxy\Common\Contracts\Models\User  $user
     * @return \Smorken\Auth\Proxy\Contracts\Models\User
     */
    public function fromResponseUser(\Smorken\Auth\Proxy\Common\Contracts\Models\User $user
    ): \Smorken\Auth\Proxy\Contracts\Models\User {
        return $this->create($user->toArray());
    }

    public function getByFilter(Filter|array $filter, int $per_page = 20): Collection
    {
        return new Collection();
    }

    public function getById(string|int $id): ?\Smorken\Auth\Proxy\Contracts\Models\User
    {
        return $this->retrieve($id);
    }

    public function getByUsername(string $username): ?\Smorken\Auth\Proxy\Contracts\Models\User
    {
        return $this->retrieve($username, 'username');
    }

    public function getCacheAssist(): ?CacheAssist
    {
        return null;
    }

    /**
     * @return \Smorken\Auth\Proxy\Contracts\Models\User
     */
    public function getModel(): \Smorken\Auth\Proxy\Contracts\Models\User
    {
        return $this->model;
    }

    public function setCacheAssist(CacheAssist $cacheAssist): void
    {
        // not used in session
    }

    public function update(string|int $id, array $data): \Smorken\Auth\Proxy\Contracts\Models\User
    {
        $data['id'] = $id;
        return $this->create($data);
    }

    public function updateFromResponseUser(\Smorken\Auth\Proxy\Common\Contracts\Models\User $user
    ): \Smorken\Auth\Proxy\Contracts\Models\User {
        return $this->fromResponseUser($user);
    }

    protected function getKey(string $identifier, string $type = 'id'): string
    {
        return sprintf('auth_user_%s_%s', $type, $identifier);
    }

    protected function getSession(): Session
    {
        return $this->session;
    }

    protected function remove(string $identifier, string $type = 'id'): void
    {
        $this->getSession()->remove($this->getKey($identifier, $type));
    }

    protected function retrieve(string $identifier, string $type = 'id'): ?\Smorken\Auth\Proxy\Contracts\Models\User
    {
        $serialized = $this->getSession()
                           ->get($this->getKey($identifier, $type));
        if ($serialized) {
            return unserialize($serialized);
        }
        return null;
    }

    protected function store(\Smorken\Auth\Proxy\Contracts\Models\User $user): void
    {
        $id_key = $this->getKey($user->id);
        $un_key = $this->getKey($user->username, 'username');
        $serialized = serialize($user);
        $this->getSession()
             ->put($id_key, $serialized);
        $this->getSession()
             ->put($un_key, $serialized);
    }
}
