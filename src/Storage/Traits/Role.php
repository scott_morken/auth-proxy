<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 7:04 AM
 */

namespace Smorken\Auth\Proxy\Storage\Traits;

use Illuminate\Support\Facades\App;
use Smorken\Roles\Contracts\Storage\RoleUser;

trait Role
{

    /**
     * @var \Smorken\Roles\Contracts\Storage\RoleUser|null|bool
     */
    protected \Smorken\Roles\Contracts\Storage\RoleUser|null|bool $roleUser = null;

    public function deleteRoles(string|int $user_id): bool
    {
        $role_user = $this->getRoleUser();
        if ($role_user) {
            return $role_user->deleteByUserId($user_id);
        }
        return true;
    }

    public function handleRoleAssignments($model, $role_id): bool
    {
        $role_user = $this->getRoleUser();
        $role_id = $this->convertToRoleId($role_id);
        if ($role_user && !is_null($role_id)) {
            return $role_user->set($model->id, $role_id);
        }
        return false;
    }

    public function hasRole($model, $role_id): bool
    {
        if (method_exists($model, 'hasRole')) {
            return $model->hasRole($role_id, false);
        }
        return false;
    }

    protected function convertToRoleId($role_id): ?int
    {
        $role_id = is_array($role_id) ? null : $role_id;
        return strlen($role_id) ? (int) $role_id : null;
    }

    /**
     * @return \Smorken\Roles\Contracts\Storage\RoleUser|false
     */
    protected function getRoleUser(): RoleUser|false
    {
        if (is_null($this->roleUser)) {
            if (App::bound(RoleUser::class)) {
                $this->roleUser = App::make(RoleUser::class);
            } else {
                $this->roleUser = false;
            }
        }
        return $this->roleUser;
    }

    public function setRoleUser(RoleUser $roleUser): void
    {
        $this->roleUser = $roleUser;
    }
}
