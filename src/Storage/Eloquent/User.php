<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 7:13 AM
 */

namespace Smorken\Auth\Proxy\Storage\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Auth\Proxy\Storage\Traits\Role;
use Smorken\CacheAssist\CacheOptions;
use Smorken\CacheAssist\Contracts\CacheAssist;
use Smorken\Support\Contracts\Filter;

class User implements \Smorken\Auth\Proxy\Contracts\Storage\User
{

    use Role;

    /**
     * @var \Smorken\CacheAssist\Contracts\CacheAssist|null
     */
    protected ?CacheAssist $cacheAssist = null;

    /**
     * @var \Smorken\Auth\Proxy\Models\Eloquent\User
     */
    protected \Smorken\Auth\Proxy\Models\Eloquent\User $model;

    public function __construct(\Smorken\Auth\Proxy\Models\Eloquent\User $user, array $cacheOptions = [])
    {
        $this->model = $user;
        if ($cacheOptions) {
            $this->cacheAssist = $this->createCacheAssist($cacheOptions);
        }
    }

    public function create(array $data): \Smorken\Auth\Proxy\Contracts\Models\User
    {
        return $this->getModel()
                    ->create($data);
    }

    /**
     * @param  \Smorken\Auth\Proxy\Contracts\Models\User  $user
     * @return bool
     */
    public function delete(\Smorken\Auth\Proxy\Contracts\Models\User $user): bool
    {
        $this->cacheForgetByModel($user);
        $this->getCacheAssist()->forgetAuto();
        if (method_exists($this, 'deleteRoles')) {
            $this->deleteRoles($user->id);
        }
        return $user->delete();
    }

    /**
     * @param  \Smorken\Auth\Proxy\Common\Contracts\Models\User  $user
     * @return \Smorken\Auth\Proxy\Contracts\Models\User
     */
    public function fromResponseUser(\Smorken\Auth\Proxy\Common\Contracts\Models\User $user
    ): \Smorken\Auth\Proxy\Contracts\Models\User {
        $data = $user->toArray();
        $id = $data['id'];
        unset($data['id']);
        $this->getCacheAssist()->forget([$id], ['username', $data['username'] ?? 'unk']);
        $this->getCacheAssist()->forgetAuto();
        return $this->getModel()
                    ->updateOrCreate(['id' => $id], $data);
    }

    /**
     * @param  \Smorken\Support\Contracts\Filter|array  $filter
     * @param  int  $per_page
     * @return \Illuminate\Support\Collection|\Illuminate\Contracts\Pagination\Paginator
     */
    public function getByFilter(Filter|array $filter, int $per_page = 20): Collection|Paginator
    {
        if (is_array($filter)) {
            $filter = new \Smorken\Support\Filter($filter);
        }
        $query = $this->queryFromFilter($filter);
        return $this->limitOrPaginate($query, $per_page);
    }

    public function getById(string|int $id): ?\Smorken\Auth\Proxy\Contracts\Models\User
    {
        return $this->getCacheAssist()->remember(
            [null, $id],
            $this->getCacheAssist()->getCacheOptions()->defaultCacheTime,
            function () use ($id) {
                try {
                    return $this->getModel()
                                ->newQuery()
                                ->idIs($id)
                                ->first();
                } catch (\Throwable) {
                    return null;
                }
            }
        );
    }

    public function getByUsername(string $username): ?\Smorken\Auth\Proxy\Contracts\Models\User
    {
        return $this->getCacheAssist()->remember(
            ['username', $username],
            $this->getCacheAssist()->getCacheOptions()->defaultCacheTime,
            function () use ($username) {
                return $this->getModel()
                            ->newQuery()
                            ->usernameIs($username)
                            ->first();
            }
        );
    }

    public function getCacheAssist(): ?CacheAssist
    {
        if (!$this->cacheAssist) {
            $this->cacheAssist = $this->createCacheAssist(['baseName' => get_class($this)]);
        }
        return $this->cacheAssist;
    }

    public function setCacheAssist(CacheAssist $cacheAssist): void
    {
        $this->cacheAssist = $cacheAssist;
    }

    /**
     * @return \Smorken\Auth\Proxy\Contracts\Models\User
     */
    public function getModel(): \Smorken\Auth\Proxy\Contracts\Models\User
    {
        return $this->model;
    }

    public function update(string|int $id, array $data): \Smorken\Auth\Proxy\Contracts\Models\User
    {
        $this->cacheForgetById($id);
        $this->cacheForgetByUsername($data['username'] ?? '');
        $this->getCacheAssist()->forgetAuto();
        return $this->getModel()
                    ->updateOrCreate(['id' => $id], $data);
    }

    public function updateFromResponseUser(\Smorken\Auth\Proxy\Common\Contracts\Models\User $user
    ): \Smorken\Auth\Proxy\Contracts\Models\User {
        $currentUser = $this->getById($user->id);
        if (!$currentUser) {
            return $this->fromResponseUser($user);
        }
        if ($this->shouldUpdateUser($user, $currentUser)) {
            $data = $user->toArray();
            unset($data['id']);
            $this->getCacheAssist()->forget([$user->id], ['username', $data['username'] ?? 'unk']);
            $currentUser->update($data);
        }
        return $currentUser;
    }

    protected function applyFilterToQuery(Filter $filter, Builder $query): Builder
    {
        $filter_methods = $this->getFilterMethods();
        foreach ($filter->all() as $key => $value) {
            if (isset($filter_methods[$key]) && method_exists($this, $filter_methods[$key])) {
                $m = $filter_methods[$key];
                $query = $this->$m($query, $value);
            }
        }
        return $query;
    }

    protected function cacheForgetById(string|int|null $id): void
    {
        if ($id !== null) {
            $id = (array) $id;
            foreach ($id as $i) {
                $this->getCacheAssist()->forget([null, $i]);
            }
        }
    }

    protected function cacheForgetByModel(\Smorken\Auth\Proxy\Contracts\Models\User $model): void
    {
        $this->cacheForgetByUsername($model->username);
        $this->cacheForgetById($model->id);
    }

    protected function cacheForgetByUsername(string $username): void
    {
        if ($username) {
            $this->getCacheAssist()->forget(['username', $username]);
        }
    }

    protected function createCacheAssist(array $cacheOptions = []): CacheAssist
    {
        if (!isset($cacheOptions['baseName'])) {
            $cacheOptions['baseName'] = get_class($this);
        }
        return new \Smorken\CacheAssist\CacheAssist(
            new CacheOptions(
                $cacheOptions
            )
        );
    }

    protected function filterFirstName(Builder $query, $value): Builder
    {
        if (strlen($value ?? '')) {
            $query = $query->firstNameLike($value);
        }
        return $query;
    }

    protected function filterId(Builder $query, $value): Builder
    {
        if (strlen($value ?? '')) {
            $query = $query->idIs($value);
        }
        return $query;
    }

    protected function filterLastName(Builder $query, $value): Builder
    {
        if (strlen($value ?? '')) {
            $query = $query->lastNameLike($value);
        }
        return $query;
    }

    protected function filterRoleIncludes(Builder $query, $value): Builder
    {
        if (strlen($value ?? '') && method_exists($this->getModel(), 'scopeRoleIncludes')) {
            $query = $query->roleIncludes($value);
        }
        return $query;
    }

    protected function filterRoleIs(Builder $query, $value): Builder
    {
        if (strlen($value ?? '') && method_exists($this->getModel(), 'scopeRoleIs')) {
            $query = $query->roleIs($value);
        }
        return $query;
    }

    protected function filterUsername(Builder $query, $value): Builder
    {
        if (strlen($value ?? '')) {
            $query = $query->usernameIs($value);
        }
        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'last_name' => 'filterLastName',
            'first_name' => 'filterFirstName',
            'id' => 'filterId',
            'username' => 'filterUsername',
            'role' => 'filterRoleIs',
        ];
    }

    /**
     * @param  Builder  $query
     * @param  int  $per_page
     * @return LengthAwarePaginator|Collection|iterable
     */
    protected function limitOrPaginate($query, int $per_page): Paginator|Collection|iterable
    {
        if ($per_page) {
            return $query->paginate($per_page);
        }
        return $query->limit(1000)
                     ->get();
    }

    /**
     * @param  Filter  $filter
     * @return Builder
     */
    protected function queryFromFilter(Filter $filter): Builder
    {
        $q = $this->getModel()
                  ->newQuery()
                  ->defaultWiths()
                  ->defaultOrder();
        return $this->applyFilterToQuery($filter, $q);
    }

    protected function shouldUpdateUser(
        \Smorken\Auth\Proxy\Common\Contracts\Models\User $responseUser,
        \Smorken\Auth\Proxy\Contracts\Models\User $user
    ): bool {
        $attrs = ['username', 'first_name', 'last_name', 'email'];
        foreach ($attrs as $attr) {
            if ($responseUser->getAttribute($attr) !== $user->getAttribute($attr)) {
                return true;
            }
        }
        return false;
    }
}
