<?php

namespace Smorken\Auth\Proxy\Services\Invokables\Admin;

use Illuminate\Contracts\Auth\Guard;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Contracts\Services\Admin\AdminFactory;
use Smorken\Auth\Proxy\Contracts\Services\Admin\DeleteService;
use Smorken\Auth\Proxy\Contracts\Services\Admin\ImpersonateService;
use Smorken\Auth\Proxy\Contracts\Services\Admin\IndexService;
use Smorken\Auth\Proxy\Contracts\Services\Admin\RetrieveService;
use Smorken\Auth\Proxy\Contracts\Services\Admin\RoleService;
use Smorken\Auth\Proxy\Contracts\Services\Admin\SaveService;
use Smorken\Auth\Proxy\Contracts\Services\Admin\SearchService;
use Smorken\Auth\Proxy\Contracts\Storage\User;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Invokables\Invokable;

class UserServices extends Invokable
{

    public function __invoke(): void
    {
        $this->getApp()->bind(AdminFactory::class, function ($app) {
            $userProvider = $app[User::class];
            $proxyProvider = $app[Provider::class];
            $filterService = new \Smorken\Auth\Proxy\Services\Admin\FilterService();
            $services = [
                DeleteService::class => new \Smorken\Auth\Proxy\Services\Admin\DeleteService(
                    $userProvider
                ),
                FilterService::class => $filterService,
                ImpersonateService::class => new \Smorken\Auth\Proxy\Services\Admin\ImpersonateService(
                    $app[Guard::class],
                    $userProvider
                ),
                IndexService::class => new \Smorken\Auth\Proxy\Services\Admin\IndexService(
                    $userProvider,
                    [
                        FilterService::class => $filterService,
                    ]
                ),
                RetrieveService::class => new \Smorken\Auth\Proxy\Services\Admin\RetrieveService(
                    $userProvider
                ),
                RoleService::class => new \Smorken\Auth\Proxy\Services\Admin\RoleService(
                    $userProvider,
                ),
                SaveService::class => new \Smorken\Auth\Proxy\Services\Admin\SaveService(
                    $userProvider,
                    $proxyProvider
                ),
                SearchService::class => new \Smorken\Auth\Proxy\Services\Admin\SearchService(
                    $proxyProvider,
                    [
                        FilterService::class => $filterService,
                    ]
                ),
            ];
            return new \Smorken\Auth\Proxy\Services\Admin\AdminFactory($services);
        });
    }
}
