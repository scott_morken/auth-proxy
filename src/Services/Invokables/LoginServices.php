<?php

namespace Smorken\Auth\Proxy\Services\Invokables;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Smorken\Auth\Proxy\Contracts\Services\Login\ExceptionService;
use Smorken\Auth\Proxy\Contracts\Services\Login\LoginFactory;
use Smorken\Service\Invokables\Invokable;

class LoginServices extends Invokable
{

    public function __invoke(): void
    {
        $this->getApp()->bind(LoginFactory::class, function ($app) {
            $services = [
                ExceptionService::class => new \Smorken\Auth\Proxy\Services\Login\ExceptionService(
                    $app[ExceptionHandler::class]
                ),
            ];
            return new \Smorken\Auth\Proxy\Services\Login\LoginFactory($services);
        });
    }
}
