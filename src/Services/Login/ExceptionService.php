<?php

namespace Smorken\Auth\Proxy\Services\Login;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Smorken\Auth\Proxy\Common\Contracts\Exception;
use Smorken\Auth\Proxy\Common\Exceptions\SystemException;
use Smorken\Auth\Proxy\Contracts\Services\Login\ExceptionResult;
use Smorken\Service\Services\BaseService;

class ExceptionService extends BaseService implements \Smorken\Auth\Proxy\Contracts\Services\Login\ExceptionService
{

    protected string $defaultError = 'There was an unhandled error during login. Please try again later.';

    protected string $voClass = \Smorken\Auth\Proxy\Services\Login\ExceptionResult::class;

    public function __construct(protected ExceptionHandler $handler, array $services = [])
    {
        parent::__construct($services);
    }

    public function getExceptionHandler(): ExceptionHandler
    {
        return $this->handler;
    }

    public function handleException(\Throwable $exception): ExceptionResult
    {
        if ($this->shouldRethrow($exception)) {
            throw $exception;
        }
        if ($this->isAuthProxyException($exception)) {
            $message = $this->handleAuthProxyException($exception);
        } else {
            $this->getExceptionHandler()->report($exception);
            $message = $this->defaultError;
        }
        return $this->newVO(['message' => $message]);
    }

    protected function handleAuthProxyException(Exception $exception): string
    {
        if ($exception instanceof SystemException) {
            $this->getExceptionHandler()->report($exception);
        }
        return $exception->display();
    }

    protected function isAuthProxyException(\Throwable $exception): bool
    {
        return $exception instanceof Exception;
    }

    protected function shouldRethrow(\Throwable $exception): bool
    {
        if ($exception instanceof ValidationException) {
            return true;
        }
        return false;
    }
}
