<?php

namespace Smorken\Auth\Proxy\Services\Login;

use Smorken\Service\Services\Factory;

class LoginFactory extends Factory implements \Smorken\Auth\Proxy\Contracts\Services\Login\LoginFactory
{
    protected array $services = [
        \Smorken\Auth\Proxy\Contracts\Services\Login\ExceptionService::class => null,
    ];
}
