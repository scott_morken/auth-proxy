<?php

namespace Smorken\Auth\Proxy\Services\Login;

use Smorken\Service\Services\VO\VOResult;

class ExceptionResult extends VOResult implements \Smorken\Auth\Proxy\Contracts\Services\Login\ExceptionResult
{

    public function __construct(public ?string $message)
    {
    }
}
