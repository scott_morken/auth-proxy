<?php

namespace Smorken\Auth\Proxy\Services\Admin;

use Smorken\Auth\Proxy\Common\Contracts\Models\Response;
use Smorken\Service\Services\VO\VOResult;
use Smorken\Support\Contracts\Filter;

class SearchResult extends VOResult implements \Smorken\Auth\Proxy\Contracts\Services\Admin\SearchResult
{

    public function __construct(
        public Filter $filter,
        public ?Response $response
    ) {
    }
}
