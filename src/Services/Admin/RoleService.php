<?php

namespace Smorken\Auth\Proxy\Services\Admin;

use Illuminate\Http\Request;
use Smorken\Auth\Proxy\Contracts\Services\Admin\RoleResult;
use Smorken\Auth\Proxy\Contracts\Storage\User;
use Smorken\Auth\Proxy\Services\Admin\Traits\HasUserProviderTrait;
use Smorken\Service\Services\BaseService;

class RoleService extends BaseService implements \Smorken\Auth\Proxy\Contracts\Services\Admin\RoleService
{

    use HasUserProviderTrait;

    protected string $voClass = \Smorken\Auth\Proxy\Services\Admin\RoleResult::class;

    public function __construct(protected User $userProvider, array $services = [])
    {
        parent::__construct($services);
    }

    public function updateRoles(Request $request, int|string $id): RoleResult
    {
        $user = $this->findModelById($id);
        $result = $this->handleRoles($request, $user);
        return $this->newVO(['user' => $user, 'result' => $result]);
    }

    protected function handleRoles(Request $request, \Smorken\Auth\Proxy\Contracts\Models\User $user): bool
    {
        if (method_exists($this->getUserProvider(), 'handleRoleAssignments')) {
            $role = $request->input('role', []);
            return $this->getUserProvider()->handleRoleAssignments($user, $role);
        }
        return false;
    }
}
