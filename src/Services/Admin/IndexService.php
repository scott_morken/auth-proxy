<?php

namespace Smorken\Auth\Proxy\Services\Admin;

use Illuminate\Http\Request;
use Smorken\Auth\Proxy\Contracts\Storage\User;
use Smorken\Service\Contracts\Services\VO\IndexResult;
use Smorken\Service\Services\BaseService;
use Smorken\Service\Services\Traits\HasFilterServiceTrait;
use Smorken\Support\Contracts\Filter;

class IndexService extends BaseService implements \Smorken\Auth\Proxy\Contracts\Services\Admin\IndexService
{

    use HasFilterServiceTrait;

    protected string $voClass = \Smorken\Service\Services\VO\IndexResult::class;

    public function __construct(protected User $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public function getByFilter(Filter $filter, int $perPage = 20): IndexResult
    {
        $models = $this->getUserProvider()->getByFilter($filter->toArray(), $perPage);
        return $this->newVO([$models, $filter]);
    }

    public function getByRequest(Request $request): IndexResult
    {
        $filter = $this->getFilterService()->getFilterFromRequest($request);
        return $this->getByFilter($filter, $this->getPerPage($request));
    }

    public function getUserProvider(): User
    {
        return $this->provider;
    }

    protected function getPerPage(Request $request): int
    {
        return 20;
    }
}
