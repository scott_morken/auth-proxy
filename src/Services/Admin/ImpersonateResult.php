<?php

namespace Smorken\Auth\Proxy\Services\Admin;

use Smorken\Auth\Proxy\Contracts\Models\User;
use Smorken\Service\Services\VO\VOResult;

class ImpersonateResult extends VOResult implements \Smorken\Auth\Proxy\Contracts\Services\Admin\ImpersonateResult
{

    public function __construct(
        public User $impersonatedUser,
        public ?string $impersonatedId,
        public string $originalId,
        public string $redirectPath = '/'
    ) {
    }

    public function canContinue(): bool
    {
        return $this->impersonatedId && (string) $this->impersonatedId === (string) $this->impersonatedUser->getAuthIdentifier();
    }
}
