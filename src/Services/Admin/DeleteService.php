<?php

namespace Smorken\Auth\Proxy\Services\Admin;

use Illuminate\Http\Request;
use Smorken\Auth\Proxy\Contracts\Storage\User;
use Smorken\Auth\Proxy\Services\Admin\Traits\HasUserProviderTrait;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Contracts\Services\VO\VOResult;
use Smorken\Service\Services\BaseService;

class DeleteService extends BaseService implements \Smorken\Auth\Proxy\Contracts\Services\Admin\DeleteService
{

    use HasUserProviderTrait;

    protected string $voClass = \Smorken\Service\Services\VO\ModelResult::class;

    public function __construct(protected User $userProvider, array $services = [])
    {
        parent::__construct($services);
    }

    public function deleteFromId(int|string $id): VOResult|ModelResult
    {
        $user = $this->findModelById($id);
        $result = $this->getUserProvider()->delete($user);
        return $this->newVO(['model' => $user, 'id' => $id, 'result' => $result]);
    }

    public function deleteFromRequest(Request $request, int|string $id): VOResult|ModelResult
    {
        return $this->deleteFromId($id);
    }
}
