<?php

namespace Smorken\Auth\Proxy\Services\Admin;

use Smorken\Auth\Proxy\Contracts\Services\Admin\IndexService;
use Smorken\Service\Services\Factory;

class AdminFactory extends Factory implements \Smorken\Auth\Proxy\Contracts\Services\Admin\AdminFactory
{

    protected array $services = [
        \Smorken\Auth\Proxy\Contracts\Services\Admin\DeleteService::class => null,
        \Smorken\Service\Contracts\Services\FilterService::class => null,
        \Smorken\Auth\Proxy\Contracts\Services\Admin\ImpersonateService::class => null,
        IndexService::class => null,
        \Smorken\Auth\Proxy\Contracts\Services\Admin\RetrieveService::class => null,
        \Smorken\Auth\Proxy\Contracts\Services\Admin\RoleService::class => null,
        \Smorken\Auth\Proxy\Contracts\Services\Admin\SaveService::class => null,
        \Smorken\Auth\Proxy\Contracts\Services\Admin\SearchService::class => null,
    ];
}
