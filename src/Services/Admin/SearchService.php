<?php

namespace Smorken\Auth\Proxy\Services\Admin;

use Illuminate\Http\Request;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Contracts\Services\Admin\SearchResult;
use Smorken\Service\Services\BaseService;
use Smorken\Service\Services\Traits\HasFilterServiceTrait;
use Smorken\Support\Contracts\Filter;

class SearchService extends BaseService implements \Smorken\Auth\Proxy\Contracts\Services\Admin\SearchService
{

    use HasFilterServiceTrait;

    protected array $searchParams = [
        'id',
        'username',
        'last_name',
        'first_name',
    ];

    protected string $voClass = \Smorken\Auth\Proxy\Services\Admin\SearchResult::class;

    public function __construct(protected Provider $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public function getProxyProvider(): Provider
    {
        return $this->provider;
    }

    public function search(Request $request): SearchResult
    {
        $filter = $this->getFilterService()->getFilterFromRequest($request);
        $response = null;
        if ($this->shouldSearch($request, $filter)) {
            $response = $this->getProxyProvider()->search($this->getSearchParams($filter));
        }
        return $this->newVO(['filter' => $filter, 'response' => $response]);
    }

    protected function getSearchParams(Filter $filter): array
    {
        $params = [];
        foreach ($this->searchParams as $key) {
            $v = $filter->getAttribute($key);
            if (!is_null($v)) {
                $params[$key] = $v;
            }
        }
        return $params;
    }

    protected function hasSearchParameters(Filter $filter): bool
    {
        return count($this->getSearchParams($filter)) > 0;
    }

    protected function searchButtonPressed(Request $request): bool
    {
        return $request->has('search-button');
    }

    protected function shouldSearch(Request $request, Filter $filter): bool
    {
        return $this->searchButtonPressed($request) && $this->hasSearchParameters($filter);
    }
}
