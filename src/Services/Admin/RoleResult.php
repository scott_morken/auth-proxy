<?php

namespace Smorken\Auth\Proxy\Services\Admin;

use Smorken\Auth\Proxy\Contracts\Models\User;
use Smorken\Service\Services\VO\VOResult;

class RoleResult extends VOResult implements \Smorken\Auth\Proxy\Contracts\Services\Admin\RoleResult
{

    public function __construct(public User $user, public bool $result)
    {
    }
}
