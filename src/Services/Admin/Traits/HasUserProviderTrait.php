<?php

namespace Smorken\Auth\Proxy\Services\Admin\Traits;

use Smorken\Auth\Proxy\Contracts\Storage\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait HasUserProviderTrait
{

    public function getUserProvider(): User
    {
        return $this->userProvider;
    }

    protected function findModelById(
        int|string $id,
        bool $shouldFail = true
    ): ?\Smorken\Auth\Proxy\Contracts\Models\User {
        $user = $this->getUserProvider()->getById($id);
        if ($shouldFail && !$user) {
            throw new NotFoundHttpException("Resource [$id] could not be located.");
        }
        return $user;
    }
}
