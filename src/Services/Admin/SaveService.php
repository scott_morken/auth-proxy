<?php

namespace Smorken\Auth\Proxy\Services\Admin;

use Illuminate\Http\Request;
use Smorken\Auth\Proxy\Common\Contracts\Models\Response;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Contracts\Storage\User;
use Smorken\Auth\Proxy\Services\Admin\Traits\HasUserProviderTrait;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\BaseService;

class SaveService extends BaseService implements \Smorken\Auth\Proxy\Contracts\Services\Admin\SaveService
{

    use HasUserProviderTrait;

    protected string $voClass = \Smorken\Service\Services\VO\ModelResult::class;

    public function __construct(protected User $userProvider, protected Provider $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public function getProxyProvider(): Provider
    {
        return $this->provider;
    }

    public function save(Request $request, int|string $id): ModelResult
    {
        $response = $this->getProxyProvider()->search(['id' => $id]);
        $user = $this->getUserFromResponse($response);
        return $this->newVO(['model' => $user, 'id' => $id, 'result' => (bool) $user]);
    }

    protected function getUserFromResponse(?Response $response): ?\Smorken\Auth\Proxy\Contracts\Models\User
    {
        if ($response && $response->users) {
            $first = $response->users[0] ?? null;
            if ($first) {
                return $this->getUserProvider()->fromResponseUser($first);
            }
        }
        return null;
    }
}
