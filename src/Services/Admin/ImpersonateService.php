<?php

namespace Smorken\Auth\Proxy\Services\Admin;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Smorken\Auth\Proxy\Contracts\Services\Admin\ImpersonateResult;
use Smorken\Auth\Proxy\Contracts\Storage\User;
use Smorken\Auth\Proxy\Services\Admin\Traits\HasUserProviderTrait;
use Smorken\Service\Services\BaseService;

class ImpersonateService extends BaseService implements \Smorken\Auth\Proxy\Contracts\Services\Admin\ImpersonateService
{

    use HasUserProviderTrait;

    protected string $voClass = \Smorken\Auth\Proxy\Services\Admin\ImpersonateResult::class;

    public function __construct(protected Guard $guard, protected User $userProvider, array $services = [])
    {
        parent::__construct($services);
    }

    public function getGuard(): Guard
    {
        return $this->guard;
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function impersonate(Request $request, int|string $id): ImpersonateResult
    {
        $this->checkAuthorized();
        $originalId = $this->getGuard()->id();
        $user = $this->findModelById($id);
        $this->getGuard()->loginUsingId($user->getAuthIdentifier());
        return $this->newVO([
            'impersonatedUser' => $user,
            'impersonatedId' => $this->getGuard()->id(),
            'originalId' => $originalId,
        ]);
    }

    protected function checkAuthorized(): void
    {
        if ($this->getGuard()->guest()) {
            throw new AuthorizationException('Authenticate.');
        }
    }
}
