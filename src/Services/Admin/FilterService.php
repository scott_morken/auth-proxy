<?php

namespace Smorken\Auth\Proxy\Services\Admin;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{

    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'id' => $request->input('id'),
            'username' => $request->input('username'),
            'last_name' => $request->input('last_name'),
            'first_name' => $request->input('first_name'),
            'role' => $request->input('role'),
            'page' => $request->input('page') ?? '1',
        ]);
    }
}
