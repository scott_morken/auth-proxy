<?php

namespace Smorken\Auth\Proxy\Services\Admin;

use Smorken\Auth\Proxy\Contracts\Storage\User;
use Smorken\Auth\Proxy\Services\Admin\Traits\HasUserProviderTrait;
use Smorken\Service\Contracts\Enums\RetrieveTypes;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Contracts\Services\VO\VOResult;
use Smorken\Service\Services\BaseService;

class RetrieveService extends BaseService implements \Smorken\Auth\Proxy\Contracts\Services\Admin\RetrieveService
{

    use HasUserProviderTrait;

    protected string $voClass = \Smorken\Service\Services\VO\ModelResult::class;

    public function __construct(protected User $userProvider, array $services = [])
    {
        parent::__construct($services);
    }

    public function findById(
        int|string $id,
        bool $shouldFail = true,
        string $type = RetrieveTypes::VIEW
    ): VOResult|ModelResult {
        $model = $this->findModelById($id, $shouldFail);
        return $this->newVO(['model' => $model, 'id' => $model?->getAuthIdentifier(), 'result' => (bool) $model]);
    }
}
