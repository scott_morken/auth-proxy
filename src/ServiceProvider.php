<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 8:24 AM
 */

namespace Smorken\Auth\Proxy;

use GuzzleHttp\Client;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Arr;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Common\Providers\Guzzle;
use Smorken\Auth\Proxy\Common\Providers\ProxyDirect;
use Smorken\Auth\Proxy\Proxies\Contracts\Proxy;
use Smorken\Auth\Proxy\Proxies\Hashers\HashHmac;
use Smorken\Auth\Proxy\Routes\AdminRoutes;
use Smorken\Auth\Proxy\Routes\AuthRoutes;
use Smorken\Auth\Proxy\Services\Invokables\Admin\UserServices;
use Smorken\Auth\Proxy\Services\Invokables\LoginServices;
use Smorken\Service\Invokables\Invoker;
use Smorken\Support\Contracts\LoadRoutes;

class ServiceProvider extends \Illuminate\Foundation\Support\Providers\AuthServiceProvider
{

    public static function rawRoutes(Application $app, string $routes = 'admin'): LoadRoutes
    {
        $map = [
            'admin' => (new AdminRoutes())
                ->setApp($app)
                ->controller($app['config']->get('auth-proxy.controllers.admin',
                    \Smorken\Auth\Proxy\Http\Controllers\Admin\User\Controller::class)),
            'auth' => (new AuthRoutes())
                ->setApp($app)
                ->controller($app['config']->get('auth-proxy.controllers.login',
                    \Smorken\Auth\Proxy\Http\Controllers\Login\Controller::class)),
        ];
        return $map[$routes];
    }

    /**
     * Bootstrap the application events.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function boot(): void
    {
        $this->bootViews();
        $this->bootConfig();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutes();
        \Illuminate\Support\Facades\Auth::provider('proxy', function ($app, array $config) {
            $p = $app[\Smorken\Auth\Proxy\Common\Contracts\Provider::class];
            $up = $app[\Smorken\Auth\Proxy\Contracts\Storage\User::class];
            return new AuthProvider($p, $up, false);
        });
        $this->defineGates();
    }

    public function loadRoutes(): void
    {
        if ($this->app['config']->get('auth-proxy.load_auth_routes', true)) {
            (new AuthRoutes())
                ->controller($this->app['config']->get('auth-proxy.controllers.login',
                    \Smorken\Auth\Proxy\Http\Controllers\Login\Controller::class))
                ->prefix($this->app['config']->get('auth-proxy.prefix', null))
                ->middleware(['web'])
                ->load($this->app);
        }
        if ($this->app['config']->get('auth-proxy.load_admin_routes', true)) {
            $prefix = implode('/',
                array_filter([$this->app['config']->get('auth-proxy.admin_route_prefix', 'admin'), 'user']));
            (new AdminRoutes())
                ->controller($this->app['config']->get('auth-proxy.controllers.admin',
                    \Smorken\Auth\Proxy\Http\Controllers\Admin\User\Controller::class))
                ->prefix($prefix)
                ->middleware($this->app['config']->get('auth-proxy.admin_route_middleware',
                    ['web', 'auth', 'can:role-admin']))
                ->load($this->app);
        }
    }

    public function register(): void
    {
        $this->registerProvider();
        $this->registerStorage();
        $this->registerServices();
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'auth-proxy');
        $this->publishes([$config => config_path('auth-proxy.php')], 'config');
    }

    protected function bootViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'auth-proxy');
        $this->publishes([__DIR__.'/../views' => resource_path('views/vendor/smorken/auth-proxy'),], 'views');
    }

    protected function defineGates(): void
    {
        if (class_exists(\Smorken\Roles\ServiceProvider::class)) {
            \Smorken\Roles\ServiceProvider::defineGates($this->app);
        } elseif (class_exists(\Smorken\SimpleAdmin\ServiceProvider::class)) {
            \Smorken\SimpleAdmin\ServiceProvider::defineGates($this->app);
        }
    }

    protected function getProvider(Application $app): Provider
    {
        $provider = $app['config']->get('auth-proxy.active_provider', Guzzle::class);
        $config = $app['config']->get('auth-proxy.provider', []);
        if ($provider === ProxyDirect::class) {
            return new ProxyDirect($this->getProxy($app), $config);
        }
        return new Guzzle(new Client(), $config);
    }

    protected function getProxy(Application $app): Proxy
    {
        $config = $app['config']->get('auth-proxy', []);
        $backend_opts = Arr::get($config,
            sprintf('proxies.%s.backend_options', \Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectory::class), []);
        $backend = new \LdapQuery\Opinionated\ActiveDirectory($backend_opts);
        $hasher = new HashHmac($app['config']->get('app.key'));
        $proxy = new \Smorken\Auth\Proxy\Proxies\Providers\ActiveDirectory($backend->getLdapQuery(), $hasher);
        $proxy->configure($config);
        return $proxy;
    }

    protected function registerProvider(): void
    {
        $this->app->bind(\Smorken\Auth\Proxy\Common\Contracts\Provider::class, function ($app) {
            return $this->getProvider($app);
        });
    }

    protected function registerServices(): void
    {
        $invoker = new Invoker([
            'invokables' => [
                UserServices::class,
                LoginServices::class,
            ],
        ], __DIR__);
        $invoker->handle($this->app);
    }

    protected function registerStorage(): void
    {
        $this->app->bind(\Smorken\Auth\Proxy\Contracts\Storage\User::class, function ($app) {
            $m = new \Smorken\Auth\Proxy\Models\Eloquent\User();
            return new \Smorken\Auth\Proxy\Storage\Eloquent\User($m);
        });
    }
}
