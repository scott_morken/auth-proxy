<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 7:21 AM
 */

namespace Smorken\Auth\Proxy\Models\Session;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Smorken\Auth\Proxy\Models\Traits\Common;
use Smorken\Model\VO;

class User extends VO implements \Smorken\Auth\Proxy\Contracts\Models\User, \Serializable
{

    use Common, Authorizable;

    public function __serialize(): array
    {
        return $this->toArray();
    }

    public function __unserialize(array $data): void
    {
        $this->setAttributes($data);
    }

    /**
     * String representation of object
     *
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize(): string
    {
        return serialize($this->toArray());
    }

    /**
     * Constructs the object
     *
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param  string  $data  <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($data): void
    {
        $this->setAttributes(unserialize($data));
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        return $this->id && $this->username;
    }
}
