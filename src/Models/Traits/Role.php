<?php

namespace Smorken\Auth\Proxy\Models\Traits;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

trait Role
{

    /**
     * @var \Smorken\Roles\Contracts\Role|null|false
     */
    protected \Smorken\Roles\Contracts\Role|null|false $role = null;

    /**
     * @return \Smorken\Roles\Contracts\Role|bool
     */
    public function getRoleHandler(): \Smorken\Roles\Contracts\Role|false
    {
        if (is_null($this->role)) {
            if (App::bound(\Smorken\Roles\Contracts\Role::class)) {
                $this->role = App::make(\Smorken\Roles\Contracts\Role::class);
            } else {
                $this->role = false;
            }
        }
        return $this->role;
    }

    /**
     * @param  \Smorken\Roles\Contracts\Models\Role  $role
     * @return bool
     */
    public function hasRole(\Smorken\Roles\Contracts\Models\Role $role): bool
    {
        $handler = $this->getRoleHandler();
        if ($handler !== false) {
            return $handler->has($role, $this->id);
        }
        return false;
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(\Smorken\Roles\Models\Eloquent\Role::class);
    }

    public function scopeRoleIncludes(Builder $query, int $role): Builder
    {
        return $query->whereHas('roles', function ($q) use ($role) {
            return $q->where('level', '>=', $role);
        });
    }

    public function scopeRoleIncludesCode(Builder $query, string $code): Builder
    {
        return $query->whereHas('roles', function ($q) use ($code) {
            $sub = \Smorken\Roles\Models\Eloquent\Role::select('level')
                                                      ->where('code', '=', $code)
                                                      ->limit(1);
            $q->where('level', '>=', DB::raw("({$sub->toSql()})"))
              ->mergeBindings($sub->getQuery());
        });
    }

    public function scopeRoleIs(Builder $query, int $role): Builder
    {
        $query = $query->whereHas('roles', function ($q) use ($role) {
            return $q->where('level', '=', $role);
        });
        if ((int) $role === 0) {
            $query = $query->orWhereDoesntHave('roles');
        }
        return $query;
    }

    public function setRoleHandler($provider): void
    {
        $this->role = $provider;
    }
}
