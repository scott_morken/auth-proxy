<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 7:09 AM
 */

namespace Smorken\Auth\Proxy\Models\Traits;

trait Common
{

    public function __toString(): string
    {
        return $this->name();
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier(): string|int
    {
        return $this->getAttribute($this->getAuthIdentifierName());
    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName(): string
    {
        return 'id';
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword(): string
    {
        return 'PROXY';
    }

    public function getLogin(): string
    {
        return $this->getAttribute($this->getLoginField());
    }

    public function getLoginField(): string
    {
        return 'username';
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string|null
     */
    public function getRememberToken(): ?string
    {
        return null;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName(): string
    {
        return 'remember_me';
    }

    public function getShortNameAttribute(): string
    {
        return $this->shortName();
    }

    public function name(): string
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value): void
    {
        return;
    }

    public function shortName(): string
    {
        $first = $this->first_name ? substr($this->first_name, 0, 1).'.' : '';
        return sprintf('%s %s', $first, $this->last_name);
    }
}
