<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 7:12 AM
 */

namespace Smorken\Auth\Proxy\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Smorken\Auth\Proxy\Models\Traits\Common;
use Smorken\Auth\Proxy\Models\Traits\Role;
use Smorken\Model\Eloquent;

class User extends Eloquent implements \Smorken\Auth\Proxy\Contracts\Models\User
{

    use Role, Common, HasFactory, Authorizable;

    public $incrementing = false;

    protected $attributes = [
        'password' => '*PROXY*',
    ];

    protected $casts = ['data' => 'array'];

    protected $fillable = ['id', 'username', 'first_name', 'last_name', 'email', 'data'];

    protected $hidden = ['password'];

    protected array $rules = [
        'id' => 'required|int',
        'username' => 'required|max:255|unique',
        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'email' => 'required|email|max:255',
    ];

    /**
     * @param  array  $data
     * @return $this
     */
    public function rehydrate(array $data)
    {
        return $this->newInstance($data);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $this->scopeOrderDefault($query);
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query;
    }

    public function scopeEmailIs(Builder $query, string $email): Builder
    {
        return $query->where('email', '=', $email);
    }

    public function scopeFirstNameLike(Builder $query, string $name): Builder
    {
        $name = $name.'%';
        return $query->where('first_name', 'LIKE', $name);
    }

    public function scopeIdIs(Builder $query, array|string|int $id): Builder
    {
        return $query->where('id', '=', $id);
    }

    public function scopeLastNameLike(Builder $query, string $name): Builder
    {
        $name = $name.'%';
        return $query->where('last_name', 'LIKE', $name);
    }

    public function scopeOrderDefault(Builder $query): Builder
    {
        return $query->orderBy('last_name')
                     ->orderBy('first_name');
    }

    public function scopeUsernameIs(Builder $query, string $username): Builder
    {
        return $query->where('username', '=', $username);
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        return $this->id && $this->username;
    }
}
