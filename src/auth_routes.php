<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/15/14
 * Time: 9:54 AM
 */

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

Route::group(
    ['prefix' => Config::get('auth-proxy.prefix', null), 'middleware' => ['web']],
    function () {
        $controller = Config::get(
            'auth-proxy.controllers.login',
            \Smorken\Auth\Proxy\Http\Controllers\Login\Controller::class
        );
        Route::get(
            'login',
            [$controller, 'showLoginForm']
        )->name('login');
        Route::post(
            'login',
            [$controller, 'login']
        );
        Route::post(
            'logout',
            [$controller, 'logout']
        )->name('logout');
        Route::get(
            'help',
            [$controller, 'help']
        )->name('login_help');
    }
);
