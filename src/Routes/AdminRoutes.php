<?php

namespace Smorken\Auth\Proxy\Routes;

use Illuminate\Routing\Router;
use Smorken\Support\LoadRoutes;

class AdminRoutes extends LoadRoutes
{

    protected function loadRoutes(Router $router): void
    {
        $router->get('/', 'index');
        $router->get('view/{id}', 'view');
        $router->get('impersonate/{id}', 'impersonate');
        $router->get('search', 'search');
        $router->get('save/{id}', 'save');
        $router->post('update/{id}', 'doUpdate');
        $router->get('delete/{id}', 'delete');
        $router->delete('delete/{id}', 'deleteDelete');
    }
}
