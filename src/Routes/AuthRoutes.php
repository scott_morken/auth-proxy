<?php

namespace Smorken\Auth\Proxy\Routes;

use Illuminate\Routing\Router;
use Smorken\Support\LoadRoutes;

class AuthRoutes extends LoadRoutes
{

    protected function loadRoutes(Router $router): void
    {
        $router->get(
            'login',
            'showLoginForm'
        )->name('login');
        $router->post(
            'login',
            'login'
        );
        $router->post(
            'logout',
            'logout'
        )->name('logout');
        $router->get(
            'help',
            'help'
        )->name('login_help');
    }
}
