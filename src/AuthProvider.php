<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 9:57 AM
 */

namespace Smorken\Auth\Proxy;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Arr;
use Smorken\Auth\Proxy\Common\Contracts\Models\Response;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Common\Exceptions\AuthenticationException;
use Smorken\Auth\Proxy\Common\Exceptions\SystemException;
use Smorken\Auth\Proxy\Common\Traits\Log;
use Smorken\Auth\Proxy\Contracts\Storage\User;

class AuthProvider implements UserProvider
{

    use Log;

    /**
     * Store authentication result from retrieveByCredentials
     * to avoid duplicate calls to provider (guard->attempt calls
     * both retrieveByCredentials and validateCredentials)
     *
     * @var bool
     */
    protected bool $authenticated = false;

    /**
     * @var \Smorken\Auth\Proxy\Common\Contracts\Provider
     */
    protected \Smorken\Auth\Proxy\Common\Contracts\Provider $provider;

    protected bool $should_authenticate = true;

    /**
     * @var \Smorken\Auth\Proxy\Contracts\Storage\User
     */
    protected \Smorken\Auth\Proxy\Contracts\Storage\User $userProvider;

    public function __construct(Provider $provider, User $userProvider, bool $debug = false)
    {
        $this->provider = $provider;
        $this->userProvider = $userProvider;
        $this->debug = $debug;
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     * @throws \Smorken\Auth\Proxy\Common\Exceptions\AuthenticationException
     * @throws \Smorken\Auth\Proxy\Common\Exceptions\SystemException
     */
    public function retrieveByCredentials(array $credentials): ?\Smorken\Auth\Proxy\Contracts\Models\User
    {
        $user = $this->getUserProvider()
                     ->getByUsername($credentials['username'] ?? null);
        if ($user) {
            return $user;
        }
        $r = $this->getProvider()
                  ->authenticate($credentials['username'] ?? null, $credentials['password'] ?? null);
        $this->should_authenticate = false;
        if ($r->isAuthenticated()) {
            $this->authenticated = true;
            return $this->userFromResponseUser($r->user);
        }
        $this->rethrowException($r);
        return null;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     * @throws \Smorken\Auth\Proxy\Common\Exceptions\AuthenticationException
     */
    public function retrieveById($identifier): ?\Smorken\Auth\Proxy\Contracts\Models\User
    {
        $user = $this->getUserProvider()
                     ->getById($identifier);
        if (!$user) {
            $response = $this->getProvider()
                             ->search(['id' => $identifier]);
            if ($response->hasUsers()) {
                $user = Arr::first($response->users);
                return $this->userFromResponseUser($user);
            } else {
                throw new AuthenticationException('Unable to find user with ID ['.$identifier.'].', 'User not found.');
            }
        }
        return $user;
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed  $identifier
     * @param  string  $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token): ?\Smorken\Auth\Proxy\Contracts\Models\User
    {
        return null;
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token): void
    {
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     * @throws \Smorken\Auth\Proxy\Common\Exceptions\AuthenticationException
     * @throws \Smorken\Auth\Proxy\Common\Exceptions\SystemException
     */
    public function validateCredentials(Authenticatable $user, array $credentials): bool
    {
        if ($this->should_authenticate) {
            $response = $this->getProvider()
                             ->authenticate($credentials['username'] ?? null, $credentials['password'] ?? null);
            if (!$response->isError()) {
                $valid = $response->isAuthenticated() &&
                    (string) $user->getAuthIdentifier() === (string) $response->user->id;
                if ($valid) {
                    $this->getUserProvider()->updateFromResponseUser($response->user);
                }
                return $valid;
            }
            $this->rethrowException($response);
        }
        return $this->authenticated;
    }

    protected function getProvider(): Provider
    {
        return $this->provider;
    }

    /**
     * @return \Smorken\Auth\Proxy\Contracts\Models\User
     */
    protected function getUserModel(): \Smorken\Auth\Proxy\Contracts\Models\User
    {
        return $this->getUserProvider()
                    ->getModel()
                    ->newInstance();
    }

    protected function getUserProvider(): User
    {
        return $this->userProvider;
    }

    protected function rethrowException(Response $response): void
    {
        if ($response->getStatus() >= 500) {
            throw new SystemException($response->message);
        }
        throw new AuthenticationException($response->message, $response->message, $response->getStatus());
    }

    /**
     * @param  \Smorken\Auth\Proxy\Common\Contracts\Models\User|null  $user
     * @return Authenticatable
     * @throws \Smorken\Auth\Proxy\Common\Exceptions\AuthenticationException
     */
    protected function userFromResponseUser(?\Smorken\Auth\Proxy\Common\Contracts\Models\User $user
    ): \Smorken\Auth\Proxy\Contracts\Models\User {
        if (!$user || ($user && !$user->validate())) {
            throw new AuthenticationException('Unable to locate user.', 'Unable to locate user.');
        }
        return $this->getUserProvider()
                    ->fromResponseUser($user);
    }
}
